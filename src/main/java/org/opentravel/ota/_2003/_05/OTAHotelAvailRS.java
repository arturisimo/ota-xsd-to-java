
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;choice&amp;gt;
 *           &amp;lt;sequence&amp;gt;
 *             &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *             &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *             &amp;lt;element name="Profiles" type="{http://www.opentravel.org/OTA/2003/05}ProfilesType" minOccurs="0"/&amp;gt;
 *             &amp;lt;element name="HotelStays" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;sequence&amp;gt;
 *                       &amp;lt;element name="HotelStay" maxOccurs="unbounded"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="Availability" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="Restriction" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                             &amp;lt;complexType&amp;gt;
 *                                               &amp;lt;complexContent&amp;gt;
 *                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                   &amp;lt;attribute name="RestrictionType"&amp;gt;
 *                                                     &amp;lt;simpleType&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                         &amp;lt;enumeration value="MinLOS"/&amp;gt;
 *                                                         &amp;lt;enumeration value="MaxLOS"/&amp;gt;
 *                                                         &amp;lt;enumeration value="FixedLOS"/&amp;gt;
 *                                                         &amp;lt;enumeration value="MinAdvanceBook"/&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/simpleType&amp;gt;
 *                                                   &amp;lt;/attribute&amp;gt;
 *                                                   &amp;lt;attribute name="Time" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                   &amp;lt;attribute name="TimeUnit" type="{http://www.opentravel.org/OTA/2003/05}TimeUnitType" /&amp;gt;
 *                                                 &amp;lt;/restriction&amp;gt;
 *                                               &amp;lt;/complexContent&amp;gt;
 *                                             &amp;lt;/complexType&amp;gt;
 *                                           &amp;lt;/element&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                         &amp;lt;attribute name="Status" use="required" type="{http://www.opentravel.org/OTA/2003/05}AvailabilityStatusType" /&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                                 &amp;lt;element name="BasicPropertyInfo" type="{http://www.opentravel.org/OTA/2003/05}BasicPropertyInfoType" minOccurs="0"/&amp;gt;
 *                                 &amp;lt;element name="Price" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                         &amp;lt;attribute name="AmountBeforeTax" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                         &amp;lt;attribute name="AmountAfterTax" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                         &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                                         &amp;lt;attribute name="Decimal" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attribute name="RoomStayRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                     &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *             &amp;lt;element name="RoomStays" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;sequence&amp;gt;
 *                       &amp;lt;element name="RoomStay" maxOccurs="unbounded"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="Reference" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                                         &amp;lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                                       &amp;lt;/extension&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                                 &amp;lt;element name="ServiceRPHs" type="{http://www.opentravel.org/OTA/2003/05}ServiceRPHsType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResponseTypeGroup"/&amp;gt;
 *                               &amp;lt;attribute name="IsAlternate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                               &amp;lt;attribute name="AvailabilityStatus" type="{http://www.opentravel.org/OTA/2003/05}RateIndicatorType" /&amp;gt;
 *                               &amp;lt;attribute name="RoomStayCandidateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                               &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                               &amp;lt;attribute name="InfoSource" type="{http://www.opentravel.org/OTA/2003/05}InfoSourceType" /&amp;gt;
 *                               &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                               &amp;lt;attribute name="AvailableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;/extension&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                     &amp;lt;/sequence&amp;gt;
 *                     &amp;lt;attribute name="MoreIndicator" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                     &amp;lt;attribute name="SortOrder" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *             &amp;lt;element name="Services" type="{http://www.opentravel.org/OTA/2003/05}ServicesType" minOccurs="0"/&amp;gt;
 *             &amp;lt;element name="Areas" type="{http://www.opentravel.org/OTA/2003/05}AreasType" minOccurs="0"/&amp;gt;
 *             &amp;lt;element name="Criteria" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;sequence&amp;gt;
 *                       &amp;lt;element name="Criterion" type="{http://www.opentravel.org/OTA/2003/05}HotelSearchCriterionType" maxOccurs="unbounded"/&amp;gt;
 *                     &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *             &amp;lt;element name="CurrencyConversions" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;sequence&amp;gt;
 *                       &amp;lt;element name="CurrencyConversion" maxOccurs="unbounded"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                               &amp;lt;attribute name="RateConversion" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                               &amp;lt;attribute name="SourceCurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                               &amp;lt;attribute name="RequestedCurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                               &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                               &amp;lt;attribute name="Source" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                     &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *             &amp;lt;element name="RebatePrograms" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;sequence&amp;gt;
 *                       &amp;lt;element name="RebateProgram" type="{http://www.opentravel.org/OTA/2003/05}RebateType" maxOccurs="unbounded"/&amp;gt;
 *                     &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *           &amp;lt;/sequence&amp;gt;
 *           &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *       &amp;lt;attribute name="SearchCacheLevel"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="Live"/&amp;gt;
 *             &amp;lt;enumeration value="VeryRecent"/&amp;gt;
 *             &amp;lt;enumeration value="LessRecent"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "success",
    "warnings",
    "profiles",
    "hotelStays",
    "roomStays",
    "services",
    "areas",
    "criteria",
    "currencyConversions",
    "rebatePrograms",
    "tpaExtensions",
    "errors"
})
@XmlRootElement(name = "OTA_HotelAvailRS")
public class OTAHotelAvailRS {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "Profiles")
    protected ProfilesType profiles;
    @XmlElement(name = "HotelStays")
    protected OTAHotelAvailRS.HotelStays hotelStays;
    @XmlElement(name = "RoomStays")
    protected OTAHotelAvailRS.RoomStays roomStays;
    @XmlElement(name = "Services")
    protected ServicesType services;
    @XmlElement(name = "Areas")
    protected AreasType areas;
    @XmlElement(name = "Criteria")
    protected OTAHotelAvailRS.Criteria criteria;
    @XmlElement(name = "CurrencyConversions")
    protected OTAHotelAvailRS.CurrencyConversions currencyConversions;
    @XmlElement(name = "RebatePrograms")
    protected OTAHotelAvailRS.RebatePrograms rebatePrograms;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "SearchCacheLevel")
    protected String searchCacheLevel;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad profiles.
     * 
     * @return
     *     possible object is
     *     {@link ProfilesType }
     *     
     */
    public ProfilesType getProfiles() {
        return profiles;
    }

    /**
     * Define el valor de la propiedad profiles.
     * 
     * @param value
     *     allowed object is
     *     {@link ProfilesType }
     *     
     */
    public void setProfiles(ProfilesType value) {
        this.profiles = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelStays.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelAvailRS.HotelStays }
     *     
     */
    public OTAHotelAvailRS.HotelStays getHotelStays() {
        return hotelStays;
    }

    /**
     * Define el valor de la propiedad hotelStays.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelAvailRS.HotelStays }
     *     
     */
    public void setHotelStays(OTAHotelAvailRS.HotelStays value) {
        this.hotelStays = value;
    }

    /**
     * Obtiene el valor de la propiedad roomStays.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelAvailRS.RoomStays }
     *     
     */
    public OTAHotelAvailRS.RoomStays getRoomStays() {
        return roomStays;
    }

    /**
     * Define el valor de la propiedad roomStays.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelAvailRS.RoomStays }
     *     
     */
    public void setRoomStays(OTAHotelAvailRS.RoomStays value) {
        this.roomStays = value;
    }

    /**
     * Obtiene el valor de la propiedad services.
     * 
     * @return
     *     possible object is
     *     {@link ServicesType }
     *     
     */
    public ServicesType getServices() {
        return services;
    }

    /**
     * Define el valor de la propiedad services.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicesType }
     *     
     */
    public void setServices(ServicesType value) {
        this.services = value;
    }

    /**
     * Obtiene el valor de la propiedad areas.
     * 
     * @return
     *     possible object is
     *     {@link AreasType }
     *     
     */
    public AreasType getAreas() {
        return areas;
    }

    /**
     * Define el valor de la propiedad areas.
     * 
     * @param value
     *     allowed object is
     *     {@link AreasType }
     *     
     */
    public void setAreas(AreasType value) {
        this.areas = value;
    }

    /**
     * Obtiene el valor de la propiedad criteria.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelAvailRS.Criteria }
     *     
     */
    public OTAHotelAvailRS.Criteria getCriteria() {
        return criteria;
    }

    /**
     * Define el valor de la propiedad criteria.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelAvailRS.Criteria }
     *     
     */
    public void setCriteria(OTAHotelAvailRS.Criteria value) {
        this.criteria = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyConversions.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelAvailRS.CurrencyConversions }
     *     
     */
    public OTAHotelAvailRS.CurrencyConversions getCurrencyConversions() {
        return currencyConversions;
    }

    /**
     * Define el valor de la propiedad currencyConversions.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelAvailRS.CurrencyConversions }
     *     
     */
    public void setCurrencyConversions(OTAHotelAvailRS.CurrencyConversions value) {
        this.currencyConversions = value;
    }

    /**
     * Obtiene el valor de la propiedad rebatePrograms.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelAvailRS.RebatePrograms }
     *     
     */
    public OTAHotelAvailRS.RebatePrograms getRebatePrograms() {
        return rebatePrograms;
    }

    /**
     * Define el valor de la propiedad rebatePrograms.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelAvailRS.RebatePrograms }
     *     
     */
    public void setRebatePrograms(OTAHotelAvailRS.RebatePrograms value) {
        this.rebatePrograms = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad searchCacheLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchCacheLevel() {
        return searchCacheLevel;
    }

    /**
     * Define el valor de la propiedad searchCacheLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchCacheLevel(String value) {
        this.searchCacheLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Criterion" type="{http://www.opentravel.org/OTA/2003/05}HotelSearchCriterionType" maxOccurs="unbounded"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "criterion"
    })
    public static class Criteria {

        @XmlElement(name = "Criterion", required = true)
        protected List<HotelSearchCriterionType> criterion;

        /**
         * Gets the value of the criterion property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the criterion property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCriterion().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link HotelSearchCriterionType }
         * 
         * 
         */
        public List<HotelSearchCriterionType> getCriterion() {
            if (criterion == null) {
                criterion = new ArrayList<HotelSearchCriterionType>();
            }
            return this.criterion;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CurrencyConversion" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="RateConversion" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                 &amp;lt;attribute name="SourceCurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *                 &amp;lt;attribute name="RequestedCurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *                 &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="Source" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "currencyConversion"
    })
    public static class CurrencyConversions {

        @XmlElement(name = "CurrencyConversion", required = true)
        protected List<OTAHotelAvailRS.CurrencyConversions.CurrencyConversion> currencyConversion;

        /**
         * Gets the value of the currencyConversion property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the currencyConversion property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCurrencyConversion().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAHotelAvailRS.CurrencyConversions.CurrencyConversion }
         * 
         * 
         */
        public List<OTAHotelAvailRS.CurrencyConversions.CurrencyConversion> getCurrencyConversion() {
            if (currencyConversion == null) {
                currencyConversion = new ArrayList<OTAHotelAvailRS.CurrencyConversions.CurrencyConversion>();
            }
            return this.currencyConversion;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="RateConversion" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *       &amp;lt;attribute name="SourceCurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *       &amp;lt;attribute name="RequestedCurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *       &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="Source" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CurrencyConversion {

            @XmlAttribute(name = "RateConversion")
            protected BigDecimal rateConversion;
            @XmlAttribute(name = "SourceCurrencyCode")
            protected String sourceCurrencyCode;
            @XmlAttribute(name = "RequestedCurrencyCode")
            protected String requestedCurrencyCode;
            @XmlAttribute(name = "DecimalPlaces")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger decimalPlaces;
            @XmlAttribute(name = "Source")
            protected String source;

            /**
             * Obtiene el valor de la propiedad rateConversion.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getRateConversion() {
                return rateConversion;
            }

            /**
             * Define el valor de la propiedad rateConversion.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setRateConversion(BigDecimal value) {
                this.rateConversion = value;
            }

            /**
             * Obtiene el valor de la propiedad sourceCurrencyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSourceCurrencyCode() {
                return sourceCurrencyCode;
            }

            /**
             * Define el valor de la propiedad sourceCurrencyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSourceCurrencyCode(String value) {
                this.sourceCurrencyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad requestedCurrencyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRequestedCurrencyCode() {
                return requestedCurrencyCode;
            }

            /**
             * Define el valor de la propiedad requestedCurrencyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRequestedCurrencyCode(String value) {
                this.requestedCurrencyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad decimalPlaces.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDecimalPlaces() {
                return decimalPlaces;
            }

            /**
             * Define el valor de la propiedad decimalPlaces.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDecimalPlaces(BigInteger value) {
                this.decimalPlaces = value;
            }

            /**
             * Obtiene el valor de la propiedad source.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSource() {
                return source;
            }

            /**
             * Define el valor de la propiedad source.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSource(String value) {
                this.source = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="HotelStay" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Availability" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Restriction" maxOccurs="5" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="RestrictionType"&amp;gt;
     *                                       &amp;lt;simpleType&amp;gt;
     *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                           &amp;lt;enumeration value="MinLOS"/&amp;gt;
     *                                           &amp;lt;enumeration value="MaxLOS"/&amp;gt;
     *                                           &amp;lt;enumeration value="FixedLOS"/&amp;gt;
     *                                           &amp;lt;enumeration value="MinAdvanceBook"/&amp;gt;
     *                                         &amp;lt;/restriction&amp;gt;
     *                                       &amp;lt;/simpleType&amp;gt;
     *                                     &amp;lt;/attribute&amp;gt;
     *                                     &amp;lt;attribute name="Time" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                     &amp;lt;attribute name="TimeUnit" type="{http://www.opentravel.org/OTA/2003/05}TimeUnitType" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Status" use="required" type="{http://www.opentravel.org/OTA/2003/05}AvailabilityStatusType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="BasicPropertyInfo" type="{http://www.opentravel.org/OTA/2003/05}BasicPropertyInfoType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Price" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="AmountBeforeTax" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                           &amp;lt;attribute name="AmountAfterTax" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                           &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *                           &amp;lt;attribute name="Decimal" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="RoomStayRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hotelStay"
    })
    public static class HotelStays {

        @XmlElement(name = "HotelStay", required = true)
        protected List<OTAHotelAvailRS.HotelStays.HotelStay> hotelStay;

        /**
         * Gets the value of the hotelStay property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the hotelStay property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getHotelStay().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAHotelAvailRS.HotelStays.HotelStay }
         * 
         * 
         */
        public List<OTAHotelAvailRS.HotelStays.HotelStay> getHotelStay() {
            if (hotelStay == null) {
                hotelStay = new ArrayList<OTAHotelAvailRS.HotelStays.HotelStay>();
            }
            return this.hotelStay;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Availability" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Restriction" maxOccurs="5" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="RestrictionType"&amp;gt;
         *                             &amp;lt;simpleType&amp;gt;
         *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                 &amp;lt;enumeration value="MinLOS"/&amp;gt;
         *                                 &amp;lt;enumeration value="MaxLOS"/&amp;gt;
         *                                 &amp;lt;enumeration value="FixedLOS"/&amp;gt;
         *                                 &amp;lt;enumeration value="MinAdvanceBook"/&amp;gt;
         *                               &amp;lt;/restriction&amp;gt;
         *                             &amp;lt;/simpleType&amp;gt;
         *                           &amp;lt;/attribute&amp;gt;
         *                           &amp;lt;attribute name="Time" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                           &amp;lt;attribute name="TimeUnit" type="{http://www.opentravel.org/OTA/2003/05}TimeUnitType" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Status" use="required" type="{http://www.opentravel.org/OTA/2003/05}AvailabilityStatusType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="BasicPropertyInfo" type="{http://www.opentravel.org/OTA/2003/05}BasicPropertyInfoType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Price" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="AmountBeforeTax" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                 &amp;lt;attribute name="AmountAfterTax" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                 &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *                 &amp;lt;attribute name="Decimal" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="RoomStayRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "availability",
            "basicPropertyInfo",
            "price"
        })
        public static class HotelStay {

            @XmlElement(name = "Availability")
            protected List<OTAHotelAvailRS.HotelStays.HotelStay.Availability> availability;
            @XmlElement(name = "BasicPropertyInfo")
            protected BasicPropertyInfoType basicPropertyInfo;
            @XmlElement(name = "Price")
            protected List<OTAHotelAvailRS.HotelStays.HotelStay.Price> price;
            @XmlAttribute(name = "RoomStayRPH")
            protected List<String> roomStayRPH;

            /**
             * Gets the value of the availability property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the availability property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAvailability().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAHotelAvailRS.HotelStays.HotelStay.Availability }
             * 
             * 
             */
            public List<OTAHotelAvailRS.HotelStays.HotelStay.Availability> getAvailability() {
                if (availability == null) {
                    availability = new ArrayList<OTAHotelAvailRS.HotelStays.HotelStay.Availability>();
                }
                return this.availability;
            }

            /**
             * Obtiene el valor de la propiedad basicPropertyInfo.
             * 
             * @return
             *     possible object is
             *     {@link BasicPropertyInfoType }
             *     
             */
            public BasicPropertyInfoType getBasicPropertyInfo() {
                return basicPropertyInfo;
            }

            /**
             * Define el valor de la propiedad basicPropertyInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link BasicPropertyInfoType }
             *     
             */
            public void setBasicPropertyInfo(BasicPropertyInfoType value) {
                this.basicPropertyInfo = value;
            }

            /**
             * Gets the value of the price property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the price property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPrice().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAHotelAvailRS.HotelStays.HotelStay.Price }
             * 
             * 
             */
            public List<OTAHotelAvailRS.HotelStays.HotelStay.Price> getPrice() {
                if (price == null) {
                    price = new ArrayList<OTAHotelAvailRS.HotelStays.HotelStay.Price>();
                }
                return this.price;
            }

            /**
             * Gets the value of the roomStayRPH property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomStayRPH property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRoomStayRPH().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getRoomStayRPH() {
                if (roomStayRPH == null) {
                    roomStayRPH = new ArrayList<String>();
                }
                return this.roomStayRPH;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Restriction" maxOccurs="5" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="RestrictionType"&amp;gt;
             *                   &amp;lt;simpleType&amp;gt;
             *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                       &amp;lt;enumeration value="MinLOS"/&amp;gt;
             *                       &amp;lt;enumeration value="MaxLOS"/&amp;gt;
             *                       &amp;lt;enumeration value="FixedLOS"/&amp;gt;
             *                       &amp;lt;enumeration value="MinAdvanceBook"/&amp;gt;
             *                     &amp;lt;/restriction&amp;gt;
             *                   &amp;lt;/simpleType&amp;gt;
             *                 &amp;lt;/attribute&amp;gt;
             *                 &amp;lt;attribute name="Time" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                 &amp;lt;attribute name="TimeUnit" type="{http://www.opentravel.org/OTA/2003/05}TimeUnitType" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="Status" use="required" type="{http://www.opentravel.org/OTA/2003/05}AvailabilityStatusType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "restriction"
            })
            public static class Availability {

                @XmlElement(name = "Restriction")
                protected List<OTAHotelAvailRS.HotelStays.HotelStay.Availability.Restriction> restriction;
                @XmlAttribute(name = "Status", required = true)
                protected AvailabilityStatusType status;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Gets the value of the restriction property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the restriction property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getRestriction().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAHotelAvailRS.HotelStays.HotelStay.Availability.Restriction }
                 * 
                 * 
                 */
                public List<OTAHotelAvailRS.HotelStays.HotelStay.Availability.Restriction> getRestriction() {
                    if (restriction == null) {
                        restriction = new ArrayList<OTAHotelAvailRS.HotelStays.HotelStay.Availability.Restriction>();
                    }
                    return this.restriction;
                }

                /**
                 * Obtiene el valor de la propiedad status.
                 * 
                 * @return
                 *     possible object is
                 *     {@link AvailabilityStatusType }
                 *     
                 */
                public AvailabilityStatusType getStatus() {
                    return status;
                }

                /**
                 * Define el valor de la propiedad status.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AvailabilityStatusType }
                 *     
                 */
                public void setStatus(AvailabilityStatusType value) {
                    this.status = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="RestrictionType"&amp;gt;
                 *         &amp;lt;simpleType&amp;gt;
                 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *             &amp;lt;enumeration value="MinLOS"/&amp;gt;
                 *             &amp;lt;enumeration value="MaxLOS"/&amp;gt;
                 *             &amp;lt;enumeration value="FixedLOS"/&amp;gt;
                 *             &amp;lt;enumeration value="MinAdvanceBook"/&amp;gt;
                 *           &amp;lt;/restriction&amp;gt;
                 *         &amp;lt;/simpleType&amp;gt;
                 *       &amp;lt;/attribute&amp;gt;
                 *       &amp;lt;attribute name="Time" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *       &amp;lt;attribute name="TimeUnit" type="{http://www.opentravel.org/OTA/2003/05}TimeUnitType" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Restriction {

                    @XmlAttribute(name = "RestrictionType")
                    protected String restrictionType;
                    @XmlAttribute(name = "Time")
                    protected BigInteger time;
                    @XmlAttribute(name = "TimeUnit")
                    protected TimeUnitType timeUnit;

                    /**
                     * Obtiene el valor de la propiedad restrictionType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getRestrictionType() {
                        return restrictionType;
                    }

                    /**
                     * Define el valor de la propiedad restrictionType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setRestrictionType(String value) {
                        this.restrictionType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad time.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getTime() {
                        return time;
                    }

                    /**
                     * Define el valor de la propiedad time.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setTime(BigInteger value) {
                        this.time = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad timeUnit.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TimeUnitType }
                     *     
                     */
                    public TimeUnitType getTimeUnit() {
                        return timeUnit;
                    }

                    /**
                     * Define el valor de la propiedad timeUnit.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TimeUnitType }
                     *     
                     */
                    public void setTimeUnit(TimeUnitType value) {
                        this.timeUnit = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="AmountBeforeTax" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *       &amp;lt;attribute name="AmountAfterTax" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *       &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
             *       &amp;lt;attribute name="Decimal" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Price {

                @XmlAttribute(name = "AmountBeforeTax")
                protected BigDecimal amountBeforeTax;
                @XmlAttribute(name = "AmountAfterTax")
                protected BigDecimal amountAfterTax;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "Decimal")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimal;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad amountBeforeTax.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmountBeforeTax() {
                    return amountBeforeTax;
                }

                /**
                 * Define el valor de la propiedad amountBeforeTax.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmountBeforeTax(BigDecimal value) {
                    this.amountBeforeTax = value;
                }

                /**
                 * Obtiene el valor de la propiedad amountAfterTax.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmountAfterTax() {
                    return amountAfterTax;
                }

                /**
                 * Define el valor de la propiedad amountAfterTax.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmountAfterTax(BigDecimal value) {
                    this.amountAfterTax = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimal() {
                    return decimal;
                }

                /**
                 * Define el valor de la propiedad decimal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimal(BigInteger value) {
                    this.decimal = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RebateProgram" type="{http://www.opentravel.org/OTA/2003/05}RebateType" maxOccurs="unbounded"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rebateProgram"
    })
    public static class RebatePrograms {

        @XmlElement(name = "RebateProgram", required = true)
        protected List<RebateType> rebateProgram;

        /**
         * Gets the value of the rebateProgram property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rebateProgram property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRebateProgram().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link RebateType }
         * 
         * 
         */
        public List<RebateType> getRebateProgram() {
            if (rebateProgram == null) {
                rebateProgram = new ArrayList<RebateType>();
            }
            return this.rebateProgram;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RoomStay" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Reference" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *                           &amp;lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ServiceRPHs" type="{http://www.opentravel.org/OTA/2003/05}ServiceRPHsType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResponseTypeGroup"/&amp;gt;
     *                 &amp;lt;attribute name="IsAlternate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="AvailabilityStatus" type="{http://www.opentravel.org/OTA/2003/05}RateIndicatorType" /&amp;gt;
     *                 &amp;lt;attribute name="RoomStayCandidateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                 &amp;lt;attribute name="InfoSource" type="{http://www.opentravel.org/OTA/2003/05}InfoSourceType" /&amp;gt;
     *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="AvailableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="MoreIndicator" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *       &amp;lt;attribute name="SortOrder" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "roomStay"
    })
    public static class RoomStays {

        @XmlElement(name = "RoomStay", required = true)
        protected List<OTAHotelAvailRS.RoomStays.RoomStay> roomStay;
        @XmlAttribute(name = "MoreIndicator")
        protected String moreIndicator;
        @XmlAttribute(name = "SortOrder")
        protected String sortOrder;

        /**
         * Gets the value of the roomStay property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomStay property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRoomStay().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAHotelAvailRS.RoomStays.RoomStay }
         * 
         * 
         */
        public List<OTAHotelAvailRS.RoomStays.RoomStay> getRoomStay() {
            if (roomStay == null) {
                roomStay = new ArrayList<OTAHotelAvailRS.RoomStays.RoomStay>();
            }
            return this.roomStay;
        }

        /**
         * Obtiene el valor de la propiedad moreIndicator.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMoreIndicator() {
            return moreIndicator;
        }

        /**
         * Define el valor de la propiedad moreIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMoreIndicator(String value) {
            this.moreIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad sortOrder.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSortOrder() {
            return sortOrder;
        }

        /**
         * Define el valor de la propiedad sortOrder.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSortOrder(String value) {
            this.sortOrder = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Reference" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
         *                 &amp;lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ServiceRPHs" type="{http://www.opentravel.org/OTA/2003/05}ServiceRPHsType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResponseTypeGroup"/&amp;gt;
         *       &amp;lt;attribute name="IsAlternate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="AvailabilityStatus" type="{http://www.opentravel.org/OTA/2003/05}RateIndicatorType" /&amp;gt;
         *       &amp;lt;attribute name="RoomStayCandidateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *       &amp;lt;attribute name="InfoSource" type="{http://www.opentravel.org/OTA/2003/05}InfoSourceType" /&amp;gt;
         *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="AvailableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "reference",
            "serviceRPHs"
        })
        public static class RoomStay
            extends RoomStayType
        {

            @XmlElement(name = "Reference")
            protected OTAHotelAvailRS.RoomStays.RoomStay.Reference reference;
            @XmlElement(name = "ServiceRPHs")
            protected ServiceRPHsType serviceRPHs;
            @XmlAttribute(name = "IsAlternate")
            protected Boolean isAlternate;
            @XmlAttribute(name = "AvailabilityStatus")
            protected RateIndicatorType availabilityStatus;
            @XmlAttribute(name = "RoomStayCandidateRPH")
            protected String roomStayCandidateRPH;
            @XmlAttribute(name = "MoreDataEchoToken")
            protected String moreDataEchoToken;
            @XmlAttribute(name = "InfoSource")
            protected String infoSource;
            @XmlAttribute(name = "RPH")
            protected String rph;
            @XmlAttribute(name = "AvailableIndicator")
            protected Boolean availableIndicator;
            @XmlAttribute(name = "ResponseType")
            protected String responseType;

            /**
             * Obtiene el valor de la propiedad reference.
             * 
             * @return
             *     possible object is
             *     {@link OTAHotelAvailRS.RoomStays.RoomStay.Reference }
             *     
             */
            public OTAHotelAvailRS.RoomStays.RoomStay.Reference getReference() {
                return reference;
            }

            /**
             * Define el valor de la propiedad reference.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAHotelAvailRS.RoomStays.RoomStay.Reference }
             *     
             */
            public void setReference(OTAHotelAvailRS.RoomStays.RoomStay.Reference value) {
                this.reference = value;
            }

            /**
             * Obtiene el valor de la propiedad serviceRPHs.
             * 
             * @return
             *     possible object is
             *     {@link ServiceRPHsType }
             *     
             */
            public ServiceRPHsType getServiceRPHs() {
                return serviceRPHs;
            }

            /**
             * Define el valor de la propiedad serviceRPHs.
             * 
             * @param value
             *     allowed object is
             *     {@link ServiceRPHsType }
             *     
             */
            public void setServiceRPHs(ServiceRPHsType value) {
                this.serviceRPHs = value;
            }

            /**
             * Obtiene el valor de la propiedad isAlternate.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isIsAlternate() {
                return isAlternate;
            }

            /**
             * Define el valor de la propiedad isAlternate.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setIsAlternate(Boolean value) {
                this.isAlternate = value;
            }

            /**
             * Obtiene el valor de la propiedad availabilityStatus.
             * 
             * @return
             *     possible object is
             *     {@link RateIndicatorType }
             *     
             */
            public RateIndicatorType getAvailabilityStatus() {
                return availabilityStatus;
            }

            /**
             * Define el valor de la propiedad availabilityStatus.
             * 
             * @param value
             *     allowed object is
             *     {@link RateIndicatorType }
             *     
             */
            public void setAvailabilityStatus(RateIndicatorType value) {
                this.availabilityStatus = value;
            }

            /**
             * Obtiene el valor de la propiedad roomStayCandidateRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRoomStayCandidateRPH() {
                return roomStayCandidateRPH;
            }

            /**
             * Define el valor de la propiedad roomStayCandidateRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRoomStayCandidateRPH(String value) {
                this.roomStayCandidateRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad moreDataEchoToken.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMoreDataEchoToken() {
                return moreDataEchoToken;
            }

            /**
             * Define el valor de la propiedad moreDataEchoToken.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMoreDataEchoToken(String value) {
                this.moreDataEchoToken = value;
            }

            /**
             * Obtiene el valor de la propiedad infoSource.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInfoSource() {
                return infoSource;
            }

            /**
             * Define el valor de la propiedad infoSource.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInfoSource(String value) {
                this.infoSource = value;
            }

            /**
             * Obtiene el valor de la propiedad rph.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRPH() {
                return rph;
            }

            /**
             * Define el valor de la propiedad rph.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRPH(String value) {
                this.rph = value;
            }

            /**
             * Obtiene el valor de la propiedad availableIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isAvailableIndicator() {
                return availableIndicator;
            }

            /**
             * Define el valor de la propiedad availableIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setAvailableIndicator(Boolean value) {
                this.availableIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad responseType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResponseType() {
                return responseType;
            }

            /**
             * Define el valor de la propiedad responseType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResponseType(String value) {
                this.responseType = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
             *       &amp;lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Reference
                extends UniqueIDType
            {

                @XmlAttribute(name = "DateTime")
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar dateTime;

                /**
                 * Obtiene el valor de la propiedad dateTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getDateTime() {
                    return dateTime;
                }

                /**
                 * Define el valor de la propiedad dateTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setDateTime(XMLGregorianCalendar value) {
                    this.dateTime = value;
                }

            }

        }

    }

}
