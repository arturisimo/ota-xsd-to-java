
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_TravelPurpose_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_TravelPurpose_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Bereavment/Emergency"/&amp;gt;
 *     &amp;lt;enumeration value="Business"/&amp;gt;
 *     &amp;lt;enumeration value="BusinessAndPersonal"/&amp;gt;
 *     &amp;lt;enumeration value="Charter/Group"/&amp;gt;
 *     &amp;lt;enumeration value="Conference/Event"/&amp;gt;
 *     &amp;lt;enumeration value="Personal"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_TravelPurpose_Base")
@XmlEnum
public enum ListTravelPurposeBase {

    @XmlEnumValue("Bereavment/Emergency")
    BEREAVMENT_EMERGENCY("Bereavment/Emergency"),
    @XmlEnumValue("Business")
    BUSINESS("Business"),
    @XmlEnumValue("BusinessAndPersonal")
    BUSINESS_AND_PERSONAL("BusinessAndPersonal"),
    @XmlEnumValue("Charter/Group")
    CHARTER_GROUP("Charter/Group"),
    @XmlEnumValue("Conference/Event")
    CONFERENCE_EVENT("Conference/Event"),
    @XmlEnumValue("Personal")
    PERSONAL("Personal"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListTravelPurposeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListTravelPurposeBase fromValue(String v) {
        for (ListTravelPurposeBase c: ListTravelPurposeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
