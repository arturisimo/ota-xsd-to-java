
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para CompartmentPositionType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="CompartmentPositionType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="CloseToRestaurantCar"/&amp;gt;
 *     &amp;lt;enumeration value="CloseToExit"/&amp;gt;
 *     &amp;lt;enumeration value="CloseToToilet"/&amp;gt;
 *     &amp;lt;enumeration value="MiddleOfCar"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "CompartmentPositionType")
@XmlEnum
public enum CompartmentPositionType {

    @XmlEnumValue("CloseToRestaurantCar")
    CLOSE_TO_RESTAURANT_CAR("CloseToRestaurantCar"),
    @XmlEnumValue("CloseToExit")
    CLOSE_TO_EXIT("CloseToExit"),
    @XmlEnumValue("CloseToToilet")
    CLOSE_TO_TOILET("CloseToToilet"),
    @XmlEnumValue("MiddleOfCar")
    MIDDLE_OF_CAR("MiddleOfCar");
    private final String value;

    CompartmentPositionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CompartmentPositionType fromValue(String v) {
        for (CompartmentPositionType c: CompartmentPositionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
