
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HotelDescriptiveInfos"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="HotelDescriptiveInfo" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelDescriptiveInfoRequestType"&amp;gt;
 *                           &amp;lt;attribute name="StateCodeList" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="CountryCodeList" type="{http://www.opentravel.org/OTA/2003/05}ListOfISO3166" /&amp;gt;
 *                           &amp;lt;attribute name="BrandCodeList" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                           &amp;lt;attribute name="ApplicableDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="LangRequested" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "hotelDescriptiveInfos"
})
@XmlRootElement(name = "OTA_HotelDescriptiveInfoRQ")
public class OTAHotelDescriptiveInfoRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "HotelDescriptiveInfos", required = true)
    protected OTAHotelDescriptiveInfoRQ.HotelDescriptiveInfos hotelDescriptiveInfos;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelDescriptiveInfos.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelDescriptiveInfoRQ.HotelDescriptiveInfos }
     *     
     */
    public OTAHotelDescriptiveInfoRQ.HotelDescriptiveInfos getHotelDescriptiveInfos() {
        return hotelDescriptiveInfos;
    }

    /**
     * Define el valor de la propiedad hotelDescriptiveInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelDescriptiveInfoRQ.HotelDescriptiveInfos }
     *     
     */
    public void setHotelDescriptiveInfos(OTAHotelDescriptiveInfoRQ.HotelDescriptiveInfos value) {
        this.hotelDescriptiveInfos = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="HotelDescriptiveInfo" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelDescriptiveInfoRequestType"&amp;gt;
     *                 &amp;lt;attribute name="StateCodeList" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="CountryCodeList" type="{http://www.opentravel.org/OTA/2003/05}ListOfISO3166" /&amp;gt;
     *                 &amp;lt;attribute name="BrandCodeList" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                 &amp;lt;attribute name="ApplicableDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="LangRequested" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hotelDescriptiveInfo"
    })
    public static class HotelDescriptiveInfos {

        @XmlElement(name = "HotelDescriptiveInfo", required = true)
        protected List<OTAHotelDescriptiveInfoRQ.HotelDescriptiveInfos.HotelDescriptiveInfo> hotelDescriptiveInfo;
        @XmlAttribute(name = "LangRequested")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "language")
        protected String langRequested;

        /**
         * Gets the value of the hotelDescriptiveInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the hotelDescriptiveInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getHotelDescriptiveInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAHotelDescriptiveInfoRQ.HotelDescriptiveInfos.HotelDescriptiveInfo }
         * 
         * 
         */
        public List<OTAHotelDescriptiveInfoRQ.HotelDescriptiveInfos.HotelDescriptiveInfo> getHotelDescriptiveInfo() {
            if (hotelDescriptiveInfo == null) {
                hotelDescriptiveInfo = new ArrayList<OTAHotelDescriptiveInfoRQ.HotelDescriptiveInfos.HotelDescriptiveInfo>();
            }
            return this.hotelDescriptiveInfo;
        }

        /**
         * Obtiene el valor de la propiedad langRequested.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLangRequested() {
            return langRequested;
        }

        /**
         * Define el valor de la propiedad langRequested.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLangRequested(String value) {
            this.langRequested = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelDescriptiveInfoRequestType"&amp;gt;
         *       &amp;lt;attribute name="StateCodeList" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="CountryCodeList" type="{http://www.opentravel.org/OTA/2003/05}ListOfISO3166" /&amp;gt;
         *       &amp;lt;attribute name="BrandCodeList" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *       &amp;lt;attribute name="ApplicableDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class HotelDescriptiveInfo
            extends HotelDescriptiveInfoRequestType
        {

            @XmlAttribute(name = "StateCodeList")
            protected List<String> stateCodeList;
            @XmlAttribute(name = "CountryCodeList")
            protected List<String> countryCodeList;
            @XmlAttribute(name = "BrandCodeList")
            protected List<String> brandCodeList;
            @XmlAttribute(name = "MoreDataEchoToken")
            protected String moreDataEchoToken;
            @XmlAttribute(name = "ApplicableDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar applicableDate;

            /**
             * Gets the value of the stateCodeList property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the stateCodeList property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getStateCodeList().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getStateCodeList() {
                if (stateCodeList == null) {
                    stateCodeList = new ArrayList<String>();
                }
                return this.stateCodeList;
            }

            /**
             * Gets the value of the countryCodeList property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the countryCodeList property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getCountryCodeList().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getCountryCodeList() {
                if (countryCodeList == null) {
                    countryCodeList = new ArrayList<String>();
                }
                return this.countryCodeList;
            }

            /**
             * Gets the value of the brandCodeList property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the brandCodeList property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getBrandCodeList().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getBrandCodeList() {
                if (brandCodeList == null) {
                    brandCodeList = new ArrayList<String>();
                }
                return this.brandCodeList;
            }

            /**
             * Obtiene el valor de la propiedad moreDataEchoToken.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMoreDataEchoToken() {
                return moreDataEchoToken;
            }

            /**
             * Define el valor de la propiedad moreDataEchoToken.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMoreDataEchoToken(String value) {
                this.moreDataEchoToken = value;
            }

            /**
             * Obtiene el valor de la propiedad applicableDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getApplicableDate() {
                return applicableDate;
            }

            /**
             * Define el valor de la propiedad applicableDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setApplicableDate(XMLGregorianCalendar value) {
                this.applicableDate = value;
            }

        }

    }

}
