
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Service location details.
 * 
 * &lt;p&gt;Clase Java para GroundLocationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GroundLocationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Address" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressInfoType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="LocationType" type="{http://www.opentravel.org/OTA/2003/05}List_GroundLocationType"/&amp;gt;
 *                   &amp;lt;element name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="SpecialInstructions" minOccurs="0"&amp;gt;
 *                     &amp;lt;simpleType&amp;gt;
 *                       &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to255"&amp;gt;
 *                       &amp;lt;/restriction&amp;gt;
 *                     &amp;lt;/simpleType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Directions" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AirportInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Arrival" type="{http://www.opentravel.org/OTA/2003/05}GroundAirportType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Departure" type="{http://www.opentravel.org/OTA/2003/05}GroundAirportType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Airline" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;OperatingAirlineType"&amp;gt;
 *                 &amp;lt;attribute name="FlightDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="DateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroundLocationType", propOrder = {
    "address",
    "airportInfo",
    "airline"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.GroundLocationsType.Stops.Stop.class,
    org.opentravel.ota._2003._05.OTAGroundModifyRQ.Reservation.Service.Locations.Stops.Stop.class,
    org.opentravel.ota._2003._05.GroundReservationSummaryType.Shuttle.ServiceLocation.class,
    org.opentravel.ota._2003._05.GroundShuttleResType.ServiceLocation.class,
    org.opentravel.ota._2003._05.GroundShuttleType.ServiceLocation.class
})
public class GroundLocationType {

    @XmlElement(name = "Address")
    protected GroundLocationType.Address address;
    @XmlElement(name = "AirportInfo")
    protected GroundLocationType.AirportInfo airportInfo;
    @XmlElement(name = "Airline")
    protected GroundLocationType.Airline airline;
    @XmlAttribute(name = "DateTime")
    protected String dateTime;

    /**
     * Obtiene el valor de la propiedad address.
     * 
     * @return
     *     possible object is
     *     {@link GroundLocationType.Address }
     *     
     */
    public GroundLocationType.Address getAddress() {
        return address;
    }

    /**
     * Define el valor de la propiedad address.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundLocationType.Address }
     *     
     */
    public void setAddress(GroundLocationType.Address value) {
        this.address = value;
    }

    /**
     * Obtiene el valor de la propiedad airportInfo.
     * 
     * @return
     *     possible object is
     *     {@link GroundLocationType.AirportInfo }
     *     
     */
    public GroundLocationType.AirportInfo getAirportInfo() {
        return airportInfo;
    }

    /**
     * Define el valor de la propiedad airportInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundLocationType.AirportInfo }
     *     
     */
    public void setAirportInfo(GroundLocationType.AirportInfo value) {
        this.airportInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad airline.
     * 
     * @return
     *     possible object is
     *     {@link GroundLocationType.Airline }
     *     
     */
    public GroundLocationType.Airline getAirline() {
        return airline;
    }

    /**
     * Define el valor de la propiedad airline.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundLocationType.Airline }
     *     
     */
    public void setAirline(GroundLocationType.Airline value) {
        this.airline = value;
    }

    /**
     * Obtiene el valor de la propiedad dateTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * Define el valor de la propiedad dateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTime(String value) {
        this.dateTime = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressInfoType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="LocationType" type="{http://www.opentravel.org/OTA/2003/05}List_GroundLocationType"/&amp;gt;
     *         &amp;lt;element name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="SpecialInstructions" minOccurs="0"&amp;gt;
     *           &amp;lt;simpleType&amp;gt;
     *             &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to255"&amp;gt;
     *             &amp;lt;/restriction&amp;gt;
     *           &amp;lt;/simpleType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Directions" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationType",
        "locationName",
        "specialInstructions",
        "directions"
    })
    public static class Address
        extends AddressInfoType
    {

        @XmlElement(name = "LocationType", required = true)
        protected ListGroundLocationType locationType;
        @XmlElement(name = "LocationName")
        protected String locationName;
        @XmlElement(name = "SpecialInstructions")
        protected String specialInstructions;
        @XmlElement(name = "Directions")
        protected FormattedTextType directions;
        @XmlAttribute(name = "Latitude")
        protected String latitude;
        @XmlAttribute(name = "Longitude")
        protected String longitude;
        @XmlAttribute(name = "Altitude")
        protected String altitude;
        @XmlAttribute(name = "AltitudeUnitOfMeasureCode")
        protected String altitudeUnitOfMeasureCode;
        @XmlAttribute(name = "PositionAccuracyCode")
        protected String positionAccuracyCode;

        /**
         * Obtiene el valor de la propiedad locationType.
         * 
         * @return
         *     possible object is
         *     {@link ListGroundLocationType }
         *     
         */
        public ListGroundLocationType getLocationType() {
            return locationType;
        }

        /**
         * Define el valor de la propiedad locationType.
         * 
         * @param value
         *     allowed object is
         *     {@link ListGroundLocationType }
         *     
         */
        public void setLocationType(ListGroundLocationType value) {
            this.locationType = value;
        }

        /**
         * Obtiene el valor de la propiedad locationName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLocationName() {
            return locationName;
        }

        /**
         * Define el valor de la propiedad locationName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLocationName(String value) {
            this.locationName = value;
        }

        /**
         * Obtiene el valor de la propiedad specialInstructions.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSpecialInstructions() {
            return specialInstructions;
        }

        /**
         * Define el valor de la propiedad specialInstructions.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSpecialInstructions(String value) {
            this.specialInstructions = value;
        }

        /**
         * Obtiene el valor de la propiedad directions.
         * 
         * @return
         *     possible object is
         *     {@link FormattedTextType }
         *     
         */
        public FormattedTextType getDirections() {
            return directions;
        }

        /**
         * Define el valor de la propiedad directions.
         * 
         * @param value
         *     allowed object is
         *     {@link FormattedTextType }
         *     
         */
        public void setDirections(FormattedTextType value) {
            this.directions = value;
        }

        /**
         * Obtiene el valor de la propiedad latitude.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLatitude() {
            return latitude;
        }

        /**
         * Define el valor de la propiedad latitude.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLatitude(String value) {
            this.latitude = value;
        }

        /**
         * Obtiene el valor de la propiedad longitude.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLongitude() {
            return longitude;
        }

        /**
         * Define el valor de la propiedad longitude.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLongitude(String value) {
            this.longitude = value;
        }

        /**
         * Obtiene el valor de la propiedad altitude.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAltitude() {
            return altitude;
        }

        /**
         * Define el valor de la propiedad altitude.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAltitude(String value) {
            this.altitude = value;
        }

        /**
         * Obtiene el valor de la propiedad altitudeUnitOfMeasureCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAltitudeUnitOfMeasureCode() {
            return altitudeUnitOfMeasureCode;
        }

        /**
         * Define el valor de la propiedad altitudeUnitOfMeasureCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAltitudeUnitOfMeasureCode(String value) {
            this.altitudeUnitOfMeasureCode = value;
        }

        /**
         * Obtiene el valor de la propiedad positionAccuracyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPositionAccuracyCode() {
            return positionAccuracyCode;
        }

        /**
         * Define el valor de la propiedad positionAccuracyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPositionAccuracyCode(String value) {
            this.positionAccuracyCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;OperatingAirlineType"&amp;gt;
     *       &amp;lt;attribute name="FlightDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Airline
        extends OperatingAirlineType
    {

        @XmlAttribute(name = "FlightDateTime")
        protected String flightDateTime;

        /**
         * Obtiene el valor de la propiedad flightDateTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightDateTime() {
            return flightDateTime;
        }

        /**
         * Define el valor de la propiedad flightDateTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightDateTime(String value) {
            this.flightDateTime = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Arrival" type="{http://www.opentravel.org/OTA/2003/05}GroundAirportType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Departure" type="{http://www.opentravel.org/OTA/2003/05}GroundAirportType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "arrival",
        "departure"
    })
    public static class AirportInfo {

        @XmlElement(name = "Arrival")
        protected GroundAirportType arrival;
        @XmlElement(name = "Departure")
        protected GroundAirportType departure;

        /**
         * Obtiene el valor de la propiedad arrival.
         * 
         * @return
         *     possible object is
         *     {@link GroundAirportType }
         *     
         */
        public GroundAirportType getArrival() {
            return arrival;
        }

        /**
         * Define el valor de la propiedad arrival.
         * 
         * @param value
         *     allowed object is
         *     {@link GroundAirportType }
         *     
         */
        public void setArrival(GroundAirportType value) {
            this.arrival = value;
        }

        /**
         * Obtiene el valor de la propiedad departure.
         * 
         * @return
         *     possible object is
         *     {@link GroundAirportType }
         *     
         */
        public GroundAirportType getDeparture() {
            return departure;
        }

        /**
         * Define el valor de la propiedad departure.
         * 
         * @param value
         *     allowed object is
         *     {@link GroundAirportType }
         *     
         */
        public void setDeparture(GroundAirportType value) {
            this.departure = value;
        }

    }

}
