
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * A listing of each seat within a cabin and the seat availability.
 * 
 * &lt;p&gt;Clase Java para CabinClassAvailabilityType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CabinClassAvailabilityType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;attribute name="SeatNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="AdvancePurchaseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="AvailableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="InoperativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="OccupiedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="PremiumInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CabinClassAvailabilityType")
public class CabinClassAvailabilityType {

    @XmlAttribute(name = "SeatNumber")
    protected String seatNumber;
    @XmlAttribute(name = "AdvancePurchaseInd")
    protected Boolean advancePurchaseInd;
    @XmlAttribute(name = "AvailableInd")
    protected Boolean availableInd;
    @XmlAttribute(name = "InoperativeInd")
    protected Boolean inoperativeInd;
    @XmlAttribute(name = "OccupiedInd")
    protected Boolean occupiedInd;
    @XmlAttribute(name = "PremiumInd")
    protected Boolean premiumInd;

    /**
     * Obtiene el valor de la propiedad seatNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatNumber() {
        return seatNumber;
    }

    /**
     * Define el valor de la propiedad seatNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatNumber(String value) {
        this.seatNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad advancePurchaseInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAdvancePurchaseInd() {
        return advancePurchaseInd;
    }

    /**
     * Define el valor de la propiedad advancePurchaseInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAdvancePurchaseInd(Boolean value) {
        this.advancePurchaseInd = value;
    }

    /**
     * Obtiene el valor de la propiedad availableInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAvailableInd() {
        return availableInd;
    }

    /**
     * Define el valor de la propiedad availableInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAvailableInd(Boolean value) {
        this.availableInd = value;
    }

    /**
     * Obtiene el valor de la propiedad inoperativeInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInoperativeInd() {
        return inoperativeInd;
    }

    /**
     * Define el valor de la propiedad inoperativeInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInoperativeInd(Boolean value) {
        this.inoperativeInd = value;
    }

    /**
     * Obtiene el valor de la propiedad occupiedInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOccupiedInd() {
        return occupiedInd;
    }

    /**
     * Define el valor de la propiedad occupiedInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOccupiedInd(Boolean value) {
        this.occupiedInd = value;
    }

    /**
     * Obtiene el valor de la propiedad premiumInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPremiumInd() {
        return premiumInd;
    }

    /**
     * Define el valor de la propiedad premiumInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPremiumInd(Boolean value) {
        this.premiumInd = value;
    }

}
