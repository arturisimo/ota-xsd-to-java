
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Describes the row details in a seat map.
 * 
 * &lt;p&gt;Clase Java para RowDetailType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RowDetailType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Characteristics" type="{http://www.opentravel.org/OTA/2003/05}RowCharacteristicsType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SeatInfo" type="{http://www.opentravel.org/OTA/2003/05}SeatDetailsType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="OperableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="PlaneSection" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="GridNumber" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="BufferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="CabinType" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
 *       &amp;lt;attribute name="RowNumber" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RowDetailType", propOrder = {
    "characteristics",
    "description",
    "seatInfo",
    "tpaExtensions"
})
public class RowDetailType {

    @XmlElement(name = "Characteristics")
    protected List<RowCharacteristicsType> characteristics;
    @XmlElement(name = "Description")
    protected List<FormattedTextTextType> description;
    @XmlElement(name = "SeatInfo")
    protected List<SeatDetailsType> seatInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "OperableInd")
    protected Boolean operableInd;
    @XmlAttribute(name = "PlaneSection")
    protected String planeSection;
    @XmlAttribute(name = "GridNumber")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger gridNumber;
    @XmlAttribute(name = "BufferInd")
    protected Boolean bufferInd;
    @XmlAttribute(name = "CabinType")
    protected String cabinType;
    @XmlAttribute(name = "RowNumber")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger rowNumber;

    /**
     * Gets the value of the characteristics property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the characteristics property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCharacteristics().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link RowCharacteristicsType }
     * 
     * 
     */
    public List<RowCharacteristicsType> getCharacteristics() {
        if (characteristics == null) {
            characteristics = new ArrayList<RowCharacteristicsType>();
        }
        return this.characteristics;
    }

    /**
     * Gets the value of the description property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the description property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getDescription().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FormattedTextTextType }
     * 
     * 
     */
    public List<FormattedTextTextType> getDescription() {
        if (description == null) {
            description = new ArrayList<FormattedTextTextType>();
        }
        return this.description;
    }

    /**
     * Gets the value of the seatInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the seatInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSeatInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link SeatDetailsType }
     * 
     * 
     */
    public List<SeatDetailsType> getSeatInfo() {
        if (seatInfo == null) {
            seatInfo = new ArrayList<SeatDetailsType>();
        }
        return this.seatInfo;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad operableInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOperableInd() {
        return operableInd;
    }

    /**
     * Define el valor de la propiedad operableInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOperableInd(Boolean value) {
        this.operableInd = value;
    }

    /**
     * Obtiene el valor de la propiedad planeSection.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaneSection() {
        return planeSection;
    }

    /**
     * Define el valor de la propiedad planeSection.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaneSection(String value) {
        this.planeSection = value;
    }

    /**
     * Obtiene el valor de la propiedad gridNumber.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getGridNumber() {
        return gridNumber;
    }

    /**
     * Define el valor de la propiedad gridNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setGridNumber(BigInteger value) {
        this.gridNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad bufferInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBufferInd() {
        return bufferInd;
    }

    /**
     * Define el valor de la propiedad bufferInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBufferInd(Boolean value) {
        this.bufferInd = value;
    }

    /**
     * Obtiene el valor de la propiedad cabinType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabinType() {
        return cabinType;
    }

    /**
     * Define el valor de la propiedad cabinType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabinType(String value) {
        this.cabinType = value;
    }

    /**
     * Obtiene el valor de la propiedad rowNumber.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRowNumber() {
        return rowNumber;
    }

    /**
     * Define el valor de la propiedad rowNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRowNumber(BigInteger value) {
        this.rowNumber = value;
    }

}
