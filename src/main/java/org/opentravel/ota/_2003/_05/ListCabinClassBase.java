
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_CabinClass_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_CabinClass_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Business"/&amp;gt;
 *     &amp;lt;enumeration value="Cockpit"/&amp;gt;
 *     &amp;lt;enumeration value="Economy"/&amp;gt;
 *     &amp;lt;enumeration value="First"/&amp;gt;
 *     &amp;lt;enumeration value="PremiumBusiness"/&amp;gt;
 *     &amp;lt;enumeration value="PremiumEconomy"/&amp;gt;
 *     &amp;lt;enumeration value="Suite"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_CabinClass_Base")
@XmlEnum
public enum ListCabinClassBase {


    /**
     * Business class compartment.
     * 
     */
    @XmlEnumValue("Business")
    BUSINESS("Business"),

    /**
     * Cockpit area.
     * 
     */
    @XmlEnumValue("Cockpit")
    COCKPIT("Cockpit"),

    /**
     * Economy class compartment.
     * 
     */
    @XmlEnumValue("Economy")
    ECONOMY("Economy"),

    /**
     * First class compartment.
     * 
     */
    @XmlEnumValue("First")
    FIRST("First"),

    /**
     * Premium Business class compartment.
     * 
     */
    @XmlEnumValue("PremiumBusiness")
    PREMIUM_BUSINESS("PremiumBusiness"),

    /**
     * Premium Economy class compartment.
     * 
     */
    @XmlEnumValue("PremiumEconomy")
    PREMIUM_ECONOMY("PremiumEconomy"),

    /**
     * Suites or Sleep compartments.
     * 
     */
    @XmlEnumValue("Suite")
    SUITE("Suite"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListCabinClassBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListCabinClassBase fromValue(String v) {
        for (ListCabinClassBase c: ListCabinClassBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
