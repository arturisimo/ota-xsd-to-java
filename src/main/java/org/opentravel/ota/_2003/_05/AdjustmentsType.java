
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * AdjustmentsType.
 * 
 * &lt;p&gt;Clase Java para AdjustmentsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AdjustmentsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Adjustment" maxOccurs="unbounded"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PromotionCodeGroup"/&amp;gt;
 *                 &amp;lt;attribute name="ReservationOriginatorCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="ConfirmationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="ReservationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="RoomInventoryCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="AdjustReason" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                 &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="InvValue"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&amp;gt;
 *                       &amp;lt;enumeration value="1"/&amp;gt;
 *                       &amp;lt;enumeration value="-1"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="RequestID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdjustmentsType", propOrder = {
    "adjustment"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAHotelInvAdjustRQ.Adjustments.class,
    org.opentravel.ota._2003._05.OTAHotelInvSyncRQ.Adjustments.class
})
public class AdjustmentsType {

    @XmlElement(name = "Adjustment", required = true)
    protected List<AdjustmentsType.Adjustment> adjustment;
    @XmlAttribute(name = "RequestID")
    protected String requestID;

    /**
     * Gets the value of the adjustment property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the adjustment property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAdjustment().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AdjustmentsType.Adjustment }
     * 
     * 
     */
    public List<AdjustmentsType.Adjustment> getAdjustment() {
        if (adjustment == null) {
            adjustment = new ArrayList<AdjustmentsType.Adjustment>();
        }
        return this.adjustment;
    }

    /**
     * Obtiene el valor de la propiedad requestID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Define el valor de la propiedad requestID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PromotionCodeGroup"/&amp;gt;
     *       &amp;lt;attribute name="ReservationOriginatorCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="ConfirmationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="ReservationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="RoomInventoryCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="AdjustReason" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *       &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="InvValue"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&amp;gt;
     *             &amp;lt;enumeration value="1"/&amp;gt;
     *             &amp;lt;enumeration value="-1"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Adjustment {

        @XmlAttribute(name = "ReservationOriginatorCode")
        protected String reservationOriginatorCode;
        @XmlAttribute(name = "ConfirmationID")
        protected String confirmationID;
        @XmlAttribute(name = "ReservationID")
        protected String reservationID;
        @XmlAttribute(name = "RoomInventoryCode")
        protected String roomInventoryCode;
        @XmlAttribute(name = "AdjustReason")
        protected String adjustReason;
        @XmlAttribute(name = "Sequence")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger sequence;
        @XmlAttribute(name = "InvValue")
        protected BigInteger invValue;
        @XmlAttribute(name = "Start")
        protected String start;
        @XmlAttribute(name = "Duration")
        protected String duration;
        @XmlAttribute(name = "End")
        protected String end;
        @XmlAttribute(name = "PromotionCode")
        protected String promotionCode;
        @XmlAttribute(name = "PromotionVendorCode")
        protected List<String> promotionVendorCode;

        /**
         * Obtiene el valor de la propiedad reservationOriginatorCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReservationOriginatorCode() {
            return reservationOriginatorCode;
        }

        /**
         * Define el valor de la propiedad reservationOriginatorCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReservationOriginatorCode(String value) {
            this.reservationOriginatorCode = value;
        }

        /**
         * Obtiene el valor de la propiedad confirmationID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfirmationID() {
            return confirmationID;
        }

        /**
         * Define el valor de la propiedad confirmationID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfirmationID(String value) {
            this.confirmationID = value;
        }

        /**
         * Obtiene el valor de la propiedad reservationID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReservationID() {
            return reservationID;
        }

        /**
         * Define el valor de la propiedad reservationID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReservationID(String value) {
            this.reservationID = value;
        }

        /**
         * Obtiene el valor de la propiedad roomInventoryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRoomInventoryCode() {
            return roomInventoryCode;
        }

        /**
         * Define el valor de la propiedad roomInventoryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRoomInventoryCode(String value) {
            this.roomInventoryCode = value;
        }

        /**
         * Obtiene el valor de la propiedad adjustReason.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAdjustReason() {
            return adjustReason;
        }

        /**
         * Define el valor de la propiedad adjustReason.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAdjustReason(String value) {
            this.adjustReason = value;
        }

        /**
         * Obtiene el valor de la propiedad sequence.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSequence() {
            return sequence;
        }

        /**
         * Define el valor de la propiedad sequence.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSequence(BigInteger value) {
            this.sequence = value;
        }

        /**
         * Obtiene el valor de la propiedad invValue.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getInvValue() {
            return invValue;
        }

        /**
         * Define el valor de la propiedad invValue.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setInvValue(BigInteger value) {
            this.invValue = value;
        }

        /**
         * Obtiene el valor de la propiedad start.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStart() {
            return start;
        }

        /**
         * Define el valor de la propiedad start.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStart(String value) {
            this.start = value;
        }

        /**
         * Obtiene el valor de la propiedad duration.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDuration() {
            return duration;
        }

        /**
         * Define el valor de la propiedad duration.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDuration(String value) {
            this.duration = value;
        }

        /**
         * Obtiene el valor de la propiedad end.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnd() {
            return end;
        }

        /**
         * Define el valor de la propiedad end.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnd(String value) {
            this.end = value;
        }

        /**
         * Obtiene el valor de la propiedad promotionCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPromotionCode() {
            return promotionCode;
        }

        /**
         * Define el valor de la propiedad promotionCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPromotionCode(String value) {
            this.promotionCode = value;
        }

        /**
         * Gets the value of the promotionVendorCode property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotionVendorCode property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPromotionVendorCode().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getPromotionVendorCode() {
            if (promotionVendorCode == null) {
                promotionVendorCode = new ArrayList<String>();
            }
            return this.promotionVendorCode;
        }

    }

}
