
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Response data for any valid item of inventory.
 * 
 * &lt;p&gt;Clase Java para ItineraryItemResponseType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ItineraryItemResponseType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;element name="Accommodation"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Identity" type="{http://www.opentravel.org/OTA/2003/05}PropertyIdentityType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="AccommodationClass" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AccommodationClassGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="DateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"/&amp;gt;
 *                   &amp;lt;element name="RoomProfiles" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="RoomProfile" maxOccurs="9"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomProfileType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Prices" maxOccurs="9" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Price" maxOccurs="9"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PriceGroup"/&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;attribute name="SupplementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;attribute name="MealPlanRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="MealPlans" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="MealPlan" type="{http://www.opentravel.org/OTA/2003/05}MealPlanType" maxOccurs="9"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DestinationLevelGroup"/&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="ResortName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                 &amp;lt;attribute name="ResortCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Flight" type="{http://www.opentravel.org/OTA/2003/05}PkgFlightSegmentType"/&amp;gt;
 *         &amp;lt;element name="RentalCar"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleRentalCoreType"&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PackageItemPositionGroup"/&amp;gt;
 *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineraryItemResponseType", propOrder = {
    "accommodation",
    "flight",
    "rentalCar"
})
public class ItineraryItemResponseType {

    @XmlElement(name = "Accommodation")
    protected ItineraryItemResponseType.Accommodation accommodation;
    @XmlElement(name = "Flight")
    protected PkgFlightSegmentType flight;
    @XmlElement(name = "RentalCar")
    protected ItineraryItemResponseType.RentalCar rentalCar;
    @XmlAttribute(name = "RPH")
    protected String rph;
    @XmlAttribute(name = "ItinerarySequence")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger itinerarySequence;
    @XmlAttribute(name = "ChronologicalSequence")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger chronologicalSequence;

    /**
     * Obtiene el valor de la propiedad accommodation.
     * 
     * @return
     *     possible object is
     *     {@link ItineraryItemResponseType.Accommodation }
     *     
     */
    public ItineraryItemResponseType.Accommodation getAccommodation() {
        return accommodation;
    }

    /**
     * Define el valor de la propiedad accommodation.
     * 
     * @param value
     *     allowed object is
     *     {@link ItineraryItemResponseType.Accommodation }
     *     
     */
    public void setAccommodation(ItineraryItemResponseType.Accommodation value) {
        this.accommodation = value;
    }

    /**
     * Obtiene el valor de la propiedad flight.
     * 
     * @return
     *     possible object is
     *     {@link PkgFlightSegmentType }
     *     
     */
    public PkgFlightSegmentType getFlight() {
        return flight;
    }

    /**
     * Define el valor de la propiedad flight.
     * 
     * @param value
     *     allowed object is
     *     {@link PkgFlightSegmentType }
     *     
     */
    public void setFlight(PkgFlightSegmentType value) {
        this.flight = value;
    }

    /**
     * Obtiene el valor de la propiedad rentalCar.
     * 
     * @return
     *     possible object is
     *     {@link ItineraryItemResponseType.RentalCar }
     *     
     */
    public ItineraryItemResponseType.RentalCar getRentalCar() {
        return rentalCar;
    }

    /**
     * Define el valor de la propiedad rentalCar.
     * 
     * @param value
     *     allowed object is
     *     {@link ItineraryItemResponseType.RentalCar }
     *     
     */
    public void setRentalCar(ItineraryItemResponseType.RentalCar value) {
        this.rentalCar = value;
    }

    /**
     * Obtiene el valor de la propiedad rph.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Define el valor de la propiedad rph.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }

    /**
     * Obtiene el valor de la propiedad itinerarySequence.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getItinerarySequence() {
        return itinerarySequence;
    }

    /**
     * Define el valor de la propiedad itinerarySequence.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setItinerarySequence(BigInteger value) {
        this.itinerarySequence = value;
    }

    /**
     * Obtiene el valor de la propiedad chronologicalSequence.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getChronologicalSequence() {
        return chronologicalSequence;
    }

    /**
     * Define el valor de la propiedad chronologicalSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setChronologicalSequence(BigInteger value) {
        this.chronologicalSequence = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Identity" type="{http://www.opentravel.org/OTA/2003/05}PropertyIdentityType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="AccommodationClass" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AccommodationClassGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="DateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"/&amp;gt;
     *         &amp;lt;element name="RoomProfiles" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="RoomProfile" maxOccurs="9"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomProfileType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Prices" maxOccurs="9" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Price" maxOccurs="9"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PriceGroup"/&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="SupplementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="MealPlanRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="MealPlans" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="MealPlan" type="{http://www.opentravel.org/OTA/2003/05}MealPlanType" maxOccurs="9"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DestinationLevelGroup"/&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="ResortName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *       &amp;lt;attribute name="ResortCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identity",
        "accommodationClass",
        "dateRange",
        "roomProfiles",
        "mealPlans"
    })
    public static class Accommodation {

        @XmlElement(name = "Identity")
        protected PropertyIdentityType identity;
        @XmlElement(name = "AccommodationClass")
        protected ItineraryItemResponseType.Accommodation.AccommodationClass accommodationClass;
        @XmlElement(name = "DateRange", required = true)
        protected DateTimeSpanType dateRange;
        @XmlElement(name = "RoomProfiles")
        protected ItineraryItemResponseType.Accommodation.RoomProfiles roomProfiles;
        @XmlElement(name = "MealPlans")
        protected ItineraryItemResponseType.Accommodation.MealPlans mealPlans;
        @XmlAttribute(name = "RPH")
        protected String rph;
        @XmlAttribute(name = "ResortName")
        protected String resortName;
        @XmlAttribute(name = "ResortCode")
        protected String resortCode;
        @XmlAttribute(name = "DestinationCode")
        protected String destinationCode;
        @XmlAttribute(name = "DestinationLevel")
        protected DestinationLevelType destinationLevel;
        @XmlAttribute(name = "DestinationName")
        protected String destinationName;

        /**
         * Obtiene el valor de la propiedad identity.
         * 
         * @return
         *     possible object is
         *     {@link PropertyIdentityType }
         *     
         */
        public PropertyIdentityType getIdentity() {
            return identity;
        }

        /**
         * Define el valor de la propiedad identity.
         * 
         * @param value
         *     allowed object is
         *     {@link PropertyIdentityType }
         *     
         */
        public void setIdentity(PropertyIdentityType value) {
            this.identity = value;
        }

        /**
         * Obtiene el valor de la propiedad accommodationClass.
         * 
         * @return
         *     possible object is
         *     {@link ItineraryItemResponseType.Accommodation.AccommodationClass }
         *     
         */
        public ItineraryItemResponseType.Accommodation.AccommodationClass getAccommodationClass() {
            return accommodationClass;
        }

        /**
         * Define el valor de la propiedad accommodationClass.
         * 
         * @param value
         *     allowed object is
         *     {@link ItineraryItemResponseType.Accommodation.AccommodationClass }
         *     
         */
        public void setAccommodationClass(ItineraryItemResponseType.Accommodation.AccommodationClass value) {
            this.accommodationClass = value;
        }

        /**
         * Obtiene el valor de la propiedad dateRange.
         * 
         * @return
         *     possible object is
         *     {@link DateTimeSpanType }
         *     
         */
        public DateTimeSpanType getDateRange() {
            return dateRange;
        }

        /**
         * Define el valor de la propiedad dateRange.
         * 
         * @param value
         *     allowed object is
         *     {@link DateTimeSpanType }
         *     
         */
        public void setDateRange(DateTimeSpanType value) {
            this.dateRange = value;
        }

        /**
         * Obtiene el valor de la propiedad roomProfiles.
         * 
         * @return
         *     possible object is
         *     {@link ItineraryItemResponseType.Accommodation.RoomProfiles }
         *     
         */
        public ItineraryItemResponseType.Accommodation.RoomProfiles getRoomProfiles() {
            return roomProfiles;
        }

        /**
         * Define el valor de la propiedad roomProfiles.
         * 
         * @param value
         *     allowed object is
         *     {@link ItineraryItemResponseType.Accommodation.RoomProfiles }
         *     
         */
        public void setRoomProfiles(ItineraryItemResponseType.Accommodation.RoomProfiles value) {
            this.roomProfiles = value;
        }

        /**
         * Obtiene el valor de la propiedad mealPlans.
         * 
         * @return
         *     possible object is
         *     {@link ItineraryItemResponseType.Accommodation.MealPlans }
         *     
         */
        public ItineraryItemResponseType.Accommodation.MealPlans getMealPlans() {
            return mealPlans;
        }

        /**
         * Define el valor de la propiedad mealPlans.
         * 
         * @param value
         *     allowed object is
         *     {@link ItineraryItemResponseType.Accommodation.MealPlans }
         *     
         */
        public void setMealPlans(ItineraryItemResponseType.Accommodation.MealPlans value) {
            this.mealPlans = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

        /**
         * Obtiene el valor de la propiedad resortName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResortName() {
            return resortName;
        }

        /**
         * Define el valor de la propiedad resortName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResortName(String value) {
            this.resortName = value;
        }

        /**
         * Obtiene el valor de la propiedad resortCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResortCode() {
            return resortCode;
        }

        /**
         * Define el valor de la propiedad resortCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResortCode(String value) {
            this.resortCode = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationCode() {
            return destinationCode;
        }

        /**
         * Define el valor de la propiedad destinationCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationCode(String value) {
            this.destinationCode = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationLevel.
         * 
         * @return
         *     possible object is
         *     {@link DestinationLevelType }
         *     
         */
        public DestinationLevelType getDestinationLevel() {
            return destinationLevel;
        }

        /**
         * Define el valor de la propiedad destinationLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link DestinationLevelType }
         *     
         */
        public void setDestinationLevel(DestinationLevelType value) {
            this.destinationLevel = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationName() {
            return destinationName;
        }

        /**
         * Define el valor de la propiedad destinationName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationName(String value) {
            this.destinationName = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AccommodationClassGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AccommodationClass {

            @XmlAttribute(name = "Code")
            protected String code;
            @XmlAttribute(name = "Name")
            protected String name;
            @XmlAttribute(name = "NationalCode")
            protected String nationalCode;
            @XmlAttribute(name = "OfficialName")
            protected String officialName;

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad nationalCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNationalCode() {
                return nationalCode;
            }

            /**
             * Define el valor de la propiedad nationalCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNationalCode(String value) {
                this.nationalCode = value;
            }

            /**
             * Obtiene el valor de la propiedad officialName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOfficialName() {
                return officialName;
            }

            /**
             * Define el valor de la propiedad officialName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOfficialName(String value) {
                this.officialName = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="MealPlan" type="{http://www.opentravel.org/OTA/2003/05}MealPlanType" maxOccurs="9"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mealPlan"
        })
        public static class MealPlans {

            @XmlElement(name = "MealPlan", required = true)
            protected List<MealPlanType> mealPlan;

            /**
             * Gets the value of the mealPlan property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the mealPlan property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getMealPlan().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link MealPlanType }
             * 
             * 
             */
            public List<MealPlanType> getMealPlan() {
                if (mealPlan == null) {
                    mealPlan = new ArrayList<MealPlanType>();
                }
                return this.mealPlan;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="RoomProfile" maxOccurs="9"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomProfileType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Prices" maxOccurs="9" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Price" maxOccurs="9"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PriceGroup"/&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="SupplementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="MealPlanRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "roomProfile"
        })
        public static class RoomProfiles {

            @XmlElement(name = "RoomProfile", required = true)
            protected List<ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile> roomProfile;

            /**
             * Gets the value of the roomProfile property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomProfile property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRoomProfile().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile }
             * 
             * 
             */
            public List<ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile> getRoomProfile() {
                if (roomProfile == null) {
                    roomProfile = new ArrayList<ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile>();
                }
                return this.roomProfile;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomProfileType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Prices" maxOccurs="9" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Price" maxOccurs="9"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PriceGroup"/&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="SupplementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="MealPlanRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "prices"
            })
            public static class RoomProfile
                extends RoomProfileType
            {

                @XmlElement(name = "Prices")
                protected List<ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile.Prices> prices;

                /**
                 * Gets the value of the prices property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the prices property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getPrices().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile.Prices }
                 * 
                 * 
                 */
                public List<ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile.Prices> getPrices() {
                    if (prices == null) {
                        prices = new ArrayList<ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile.Prices>();
                    }
                    return this.prices;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Price" maxOccurs="9"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PriceGroup"/&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="SupplementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="MealPlanRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "price"
                })
                public static class Prices {

                    @XmlElement(name = "Price", required = true)
                    protected List<ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile.Prices.Price> price;
                    @XmlAttribute(name = "SupplementIndicator")
                    protected Boolean supplementIndicator;
                    @XmlAttribute(name = "MealPlanRPH")
                    protected String mealPlanRPH;

                    /**
                     * Gets the value of the price property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the price property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getPrice().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile.Prices.Price }
                     * 
                     * 
                     */
                    public List<ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile.Prices.Price> getPrice() {
                        if (price == null) {
                            price = new ArrayList<ItineraryItemResponseType.Accommodation.RoomProfiles.RoomProfile.Prices.Price>();
                        }
                        return this.price;
                    }

                    /**
                     * Obtiene el valor de la propiedad supplementIndicator.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isSupplementIndicator() {
                        return supplementIndicator;
                    }

                    /**
                     * Define el valor de la propiedad supplementIndicator.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setSupplementIndicator(Boolean value) {
                        this.supplementIndicator = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad mealPlanRPH.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getMealPlanRPH() {
                        return mealPlanRPH;
                    }

                    /**
                     * Define el valor de la propiedad mealPlanRPH.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setMealPlanRPH(String value) {
                        this.mealPlanRPH = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PriceGroup"/&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Price {

                        @XmlAttribute(name = "AgeQualifyingCode")
                        protected String ageQualifyingCode;
                        @XmlAttribute(name = "Age")
                        protected Integer age;
                        @XmlAttribute(name = "Count")
                        protected Integer count;
                        @XmlAttribute(name = "AgeBucket")
                        protected String ageBucket;
                        @XmlAttribute(name = "PriceQualifier")
                        protected Integer priceQualifier;
                        @XmlAttribute(name = "PriceBasis")
                        protected PricingType priceBasis;
                        @XmlAttribute(name = "Amount")
                        protected BigDecimal amount;
                        @XmlAttribute(name = "CurrencyCode")
                        protected String currencyCode;
                        @XmlAttribute(name = "DecimalPlaces")
                        @XmlSchemaType(name = "nonNegativeInteger")
                        protected BigInteger decimalPlaces;

                        /**
                         * Obtiene el valor de la propiedad ageQualifyingCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getAgeQualifyingCode() {
                            return ageQualifyingCode;
                        }

                        /**
                         * Define el valor de la propiedad ageQualifyingCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setAgeQualifyingCode(String value) {
                            this.ageQualifyingCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad age.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Integer }
                         *     
                         */
                        public Integer getAge() {
                            return age;
                        }

                        /**
                         * Define el valor de la propiedad age.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Integer }
                         *     
                         */
                        public void setAge(Integer value) {
                            this.age = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad count.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Integer }
                         *     
                         */
                        public Integer getCount() {
                            return count;
                        }

                        /**
                         * Define el valor de la propiedad count.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Integer }
                         *     
                         */
                        public void setCount(Integer value) {
                            this.count = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad ageBucket.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getAgeBucket() {
                            return ageBucket;
                        }

                        /**
                         * Define el valor de la propiedad ageBucket.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setAgeBucket(String value) {
                            this.ageBucket = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad priceQualifier.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Integer }
                         *     
                         */
                        public Integer getPriceQualifier() {
                            return priceQualifier;
                        }

                        /**
                         * Define el valor de la propiedad priceQualifier.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Integer }
                         *     
                         */
                        public void setPriceQualifier(Integer value) {
                            this.priceQualifier = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad priceBasis.
                         * 
                         * @return
                         *     possible object is
                         *     {@link PricingType }
                         *     
                         */
                        public PricingType getPriceBasis() {
                            return priceBasis;
                        }

                        /**
                         * Define el valor de la propiedad priceBasis.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link PricingType }
                         *     
                         */
                        public void setPriceBasis(PricingType value) {
                            this.priceBasis = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad amount.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getAmount() {
                            return amount;
                        }

                        /**
                         * Define el valor de la propiedad amount.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setAmount(BigDecimal value) {
                            this.amount = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad currencyCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCurrencyCode() {
                            return currencyCode;
                        }

                        /**
                         * Define el valor de la propiedad currencyCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad decimalPlaces.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getDecimalPlaces() {
                            return decimalPlaces;
                        }

                        /**
                         * Define el valor de la propiedad decimalPlaces.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setDecimalPlaces(BigInteger value) {
                            this.decimalPlaces = value;
                        }

                    }

                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleRentalCoreType"&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RentalCar
        extends VehicleRentalCoreType
    {

        @XmlAttribute(name = "RPH")
        protected String rph;
        @XmlAttribute(name = "Name")
        protected String name;
        @XmlAttribute(name = "Code")
        protected String code;

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

        /**
         * Obtiene el valor de la propiedad name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

    }

}
