
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para TourActivityPricingTypeEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="TourActivityPricingTypeEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="PerPerson"/&amp;gt;
 *     &amp;lt;enumeration value="PerGroup"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "TourActivityPricingTypeEnum")
@XmlEnum
public enum TourActivityPricingTypeEnum {

    @XmlEnumValue("PerPerson")
    PER_PERSON("PerPerson"),
    @XmlEnumValue("PerGroup")
    PER_GROUP("PerGroup"),

    /**
     * OpenTravel Reserved words, "Other_" as enumerated value for  open enumeration support to support additional compartment type. The Value corresponding to "Other_" will be specified in the  "Value" attribute. See CompartmentType.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    TourActivityPricingTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TourActivityPricingTypeEnum fromValue(String v) {
        for (TourActivityPricingTypeEnum c: TourActivityPricingTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
