
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 *  Minimum information about ticketing required to complete the booking transaction plus eTicket number.
 * 
 * &lt;p&gt;Clase Java para TicketingInfoRS_Type complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TicketingInfoRS_Type"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TicketingInfoType"&amp;gt;
 *       &amp;lt;attribute name="eTicketNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TicketingInfoRS_Type")
@XmlSeeAlso({
    org.opentravel.ota._2003._05.PricedItineraryType.TicketingInfo.class,
    org.opentravel.ota._2003._05.PkgReservation.TicketingInfo.class,
    org.opentravel.ota._2003._05.AirComponentType.TicketingInfo.class,
    org.opentravel.ota._2003._05.ItineraryInfoType.Ticketing.class
})
public class TicketingInfoRSType
    extends TicketingInfoType
{

    @XmlAttribute(name = "eTicketNumber")
    protected String eTicketNumber;

    /**
     * Obtiene el valor de la propiedad eTicketNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getETicketNumber() {
        return eTicketNumber;
    }

    /**
     * Define el valor de la propiedad eTicketNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setETicketNumber(String value) {
        this.eTicketNumber = value;
    }

}
