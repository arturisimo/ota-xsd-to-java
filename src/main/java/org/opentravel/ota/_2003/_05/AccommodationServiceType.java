
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Types of accommodations and services available for passenger purchase on a train.
 * 
 * &lt;p&gt;Clase Java para AccommodationServiceType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AccommodationServiceType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="AccommodationDetail" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RailAccommDetailType"&amp;gt;
 *                 &amp;lt;attribute name="ReferenceTravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="ReferenceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AncillaryService" maxOccurs="99" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccommodationServiceType", propOrder = {
    "accommodationDetail",
    "ancillaryService"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.BookedTrainSegmentType.Assignment.class
})
public class AccommodationServiceType {

    @XmlElement(name = "AccommodationDetail")
    protected AccommodationServiceType.AccommodationDetail accommodationDetail;
    @XmlElement(name = "AncillaryService")
    protected List<AncillaryService> ancillaryService;

    /**
     * Obtiene el valor de la propiedad accommodationDetail.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationServiceType.AccommodationDetail }
     *     
     */
    public AccommodationServiceType.AccommodationDetail getAccommodationDetail() {
        return accommodationDetail;
    }

    /**
     * Define el valor de la propiedad accommodationDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationServiceType.AccommodationDetail }
     *     
     */
    public void setAccommodationDetail(AccommodationServiceType.AccommodationDetail value) {
        this.accommodationDetail = value;
    }

    /**
     * Gets the value of the ancillaryService property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ancillaryService property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAncillaryService().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AncillaryService }
     * 
     * 
     */
    public List<AncillaryService> getAncillaryService() {
        if (ancillaryService == null) {
            ancillaryService = new ArrayList<AncillaryService>();
        }
        return this.ancillaryService;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RailAccommDetailType"&amp;gt;
     *       &amp;lt;attribute name="ReferenceTravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="ReferenceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AccommodationDetail
        extends RailAccommDetailType
    {

        @XmlAttribute(name = "ReferenceTravelerRPH")
        protected String referenceTravelerRPH;
        @XmlAttribute(name = "ReferenceInd")
        protected Boolean referenceInd;

        /**
         * Obtiene el valor de la propiedad referenceTravelerRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferenceTravelerRPH() {
            return referenceTravelerRPH;
        }

        /**
         * Define el valor de la propiedad referenceTravelerRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferenceTravelerRPH(String value) {
            this.referenceTravelerRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad referenceInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isReferenceInd() {
            return referenceInd;
        }

        /**
         * Define el valor de la propiedad referenceInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setReferenceInd(Boolean value) {
            this.referenceInd = value;
        }

    }

}
