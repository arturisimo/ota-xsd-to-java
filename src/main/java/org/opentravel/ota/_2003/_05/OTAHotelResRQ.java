
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelResRequestType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="DonationInformation" type="{http://www.opentravel.org/OTA/2003/05}DonationType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="RebatePrograms" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RebateProgram" type="{http://www.opentravel.org/OTA/2003/05}RebateType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "donationInformation",
    "rebatePrograms"
})
@XmlRootElement(name = "OTA_HotelResRQ")
public class OTAHotelResRQ
    extends HotelResRequestType
{

    @XmlElement(name = "DonationInformation")
    protected DonationType donationInformation;
    @XmlElement(name = "RebatePrograms")
    protected OTAHotelResRQ.RebatePrograms rebatePrograms;

    /**
     * Obtiene el valor de la propiedad donationInformation.
     * 
     * @return
     *     possible object is
     *     {@link DonationType }
     *     
     */
    public DonationType getDonationInformation() {
        return donationInformation;
    }

    /**
     * Define el valor de la propiedad donationInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link DonationType }
     *     
     */
    public void setDonationInformation(DonationType value) {
        this.donationInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad rebatePrograms.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelResRQ.RebatePrograms }
     *     
     */
    public OTAHotelResRQ.RebatePrograms getRebatePrograms() {
        return rebatePrograms;
    }

    /**
     * Define el valor de la propiedad rebatePrograms.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelResRQ.RebatePrograms }
     *     
     */
    public void setRebatePrograms(OTAHotelResRQ.RebatePrograms value) {
        this.rebatePrograms = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RebateProgram" type="{http://www.opentravel.org/OTA/2003/05}RebateType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rebateProgram"
    })
    public static class RebatePrograms {

        @XmlElement(name = "RebateProgram")
        protected List<RebateType> rebateProgram;

        /**
         * Gets the value of the rebateProgram property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rebateProgram property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRebateProgram().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link RebateType }
         * 
         * 
         */
        public List<RebateType> getRebateProgram() {
            if (rebateProgram == null) {
                rebateProgram = new ArrayList<RebateType>();
            }
            return this.rebateProgram;
        }

    }

}
