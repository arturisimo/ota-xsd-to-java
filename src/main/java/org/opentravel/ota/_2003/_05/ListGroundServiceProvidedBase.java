
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_GroundServiceProvided_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_GroundServiceProvided_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AirportTransfer"/&amp;gt;
 *     &amp;lt;enumeration value="SharedTransportation"/&amp;gt;
 *     &amp;lt;enumeration value="Hourly"/&amp;gt;
 *     &amp;lt;enumeration value="Mileage"/&amp;gt;
 *     &amp;lt;enumeration value="SimpleTransfer"/&amp;gt;
 *     &amp;lt;enumeration value="Shuttle"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_GroundServiceProvided_Base")
@XmlEnum
public enum ListGroundServiceProvidedBase {

    @XmlEnumValue("AirportTransfer")
    AIRPORT_TRANSFER("AirportTransfer"),
    @XmlEnumValue("SharedTransportation")
    SHARED_TRANSPORTATION("SharedTransportation"),
    @XmlEnumValue("Hourly")
    HOURLY("Hourly"),
    @XmlEnumValue("Mileage")
    MILEAGE("Mileage"),
    @XmlEnumValue("SimpleTransfer")
    SIMPLE_TRANSFER("SimpleTransfer"),
    @XmlEnumValue("Shuttle")
    SHUTTLE("Shuttle"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListGroundServiceProvidedBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListGroundServiceProvidedBase fromValue(String v) {
        for (ListGroundServiceProvidedBase c: ListGroundServiceProvidedBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
