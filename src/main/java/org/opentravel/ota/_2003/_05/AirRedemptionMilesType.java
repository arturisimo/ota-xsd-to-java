
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Information about using redemption miles as currency.
 * 
 * &lt;p&gt;Clase Java para AirRedemptionMilesType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AirRedemptionMilesType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;attribute name="PointQty" use="required" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *       &amp;lt;attribute name="RewardCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="MoneyToMilesRatio" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="DiscountPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirRedemptionMilesType")
public class AirRedemptionMilesType {

    @XmlAttribute(name = "PointQty", required = true)
    protected BigDecimal pointQty;
    @XmlAttribute(name = "RewardCode")
    protected String rewardCode;
    @XmlAttribute(name = "MoneyToMilesRatio")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger moneyToMilesRatio;
    @XmlAttribute(name = "DiscountPercentage")
    protected BigDecimal discountPercentage;

    /**
     * Obtiene el valor de la propiedad pointQty.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPointQty() {
        return pointQty;
    }

    /**
     * Define el valor de la propiedad pointQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPointQty(BigDecimal value) {
        this.pointQty = value;
    }

    /**
     * Obtiene el valor de la propiedad rewardCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRewardCode() {
        return rewardCode;
    }

    /**
     * Define el valor de la propiedad rewardCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRewardCode(String value) {
        this.rewardCode = value;
    }

    /**
     * Obtiene el valor de la propiedad moneyToMilesRatio.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMoneyToMilesRatio() {
        return moneyToMilesRatio;
    }

    /**
     * Define el valor de la propiedad moneyToMilesRatio.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMoneyToMilesRatio(BigInteger value) {
        this.moneyToMilesRatio = value;
    }

    /**
     * Obtiene el valor de la propiedad discountPercentage.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    /**
     * Define el valor de la propiedad discountPercentage.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountPercentage(BigDecimal value) {
        this.discountPercentage = value;
    }

}
