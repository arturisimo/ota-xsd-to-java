
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_FeeTaxType_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_FeeTaxType_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AdultRollawayFee"/&amp;gt;
 *     &amp;lt;enumeration value="ApplicationFee"/&amp;gt;
 *     &amp;lt;enumeration value="Assessment/LicenseTax"/&amp;gt;
 *     &amp;lt;enumeration value="BanquetServiceFee"/&amp;gt;
 *     &amp;lt;enumeration value="BedTax"/&amp;gt;
 *     &amp;lt;enumeration value="ChildRollawayCharge"/&amp;gt;
 *     &amp;lt;enumeration value="CityHotelFee"/&amp;gt;
 *     &amp;lt;enumeration value="CityTax"/&amp;gt;
 *     &amp;lt;enumeration value="ConventionTax"/&amp;gt;
 *     &amp;lt;enumeration value="CountryTax"/&amp;gt;
 *     &amp;lt;enumeration value="CountyTax"/&amp;gt;
 *     &amp;lt;enumeration value="CribFee"/&amp;gt;
 *     &amp;lt;enumeration value="EarlyCheckoutFee"/&amp;gt;
 *     &amp;lt;enumeration value="EnergyTax"/&amp;gt;
 *     &amp;lt;enumeration value="Exempt"/&amp;gt;
 *     &amp;lt;enumeration value="ExpressHandlingFee"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraChildCharge"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraPersonCharge"/&amp;gt;
 *     &amp;lt;enumeration value="FederalTax"/&amp;gt;
 *     &amp;lt;enumeration value="FoodAndBeverageTax"/&amp;gt;
 *     &amp;lt;enumeration value="FoodAndBeverageGratuity"/&amp;gt;
 *     &amp;lt;enumeration value="GoodsAndServicesTax(GST)"/&amp;gt;
 *     &amp;lt;enumeration value="InsurancePremiumTax"/&amp;gt;
 *     &amp;lt;enumeration value="LocalFee"/&amp;gt;
 *     &amp;lt;enumeration value="LodgingTax"/&amp;gt;
 *     &amp;lt;enumeration value="MaintenanceFee"/&amp;gt;
 *     &amp;lt;enumeration value="Miscellaneous"/&amp;gt;
 *     &amp;lt;enumeration value="NationalGovernmentTax"/&amp;gt;
 *     &amp;lt;enumeration value="NotKnown"/&amp;gt;
 *     &amp;lt;enumeration value="OccupancyTax"/&amp;gt;
 *     &amp;lt;enumeration value="PackageFee"/&amp;gt;
 *     &amp;lt;enumeration value="PetSanitationFee"/&amp;gt;
 *     &amp;lt;enumeration value="ResortFee"/&amp;gt;
 *     &amp;lt;enumeration value="RollawayFee"/&amp;gt;
 *     &amp;lt;enumeration value="RoomServiceFee"/&amp;gt;
 *     &amp;lt;enumeration value="RoomTax"/&amp;gt;
 *     &amp;lt;enumeration value="SalesTax"/&amp;gt;
 *     &amp;lt;enumeration value="ServiceCharge"/&amp;gt;
 *     &amp;lt;enumeration value="Standard"/&amp;gt;
 *     &amp;lt;enumeration value="StateTax"/&amp;gt;
 *     &amp;lt;enumeration value="Surcharge"/&amp;gt;
 *     &amp;lt;enumeration value="SurplusLinesTax"/&amp;gt;
 *     &amp;lt;enumeration value="TotalTax"/&amp;gt;
 *     &amp;lt;enumeration value="TourismTax"/&amp;gt;
 *     &amp;lt;enumeration value="ValueAddedTax(VAT)"/&amp;gt;
 *     &amp;lt;enumeration value="VAT/GSTTax"/&amp;gt;
 *     &amp;lt;enumeration value="Zero-Rated"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_FeeTaxType_Base")
@XmlEnum
public enum ListFeeTaxTypeBase {

    @XmlEnumValue("AdultRollawayFee")
    ADULT_ROLLAWAY_FEE("AdultRollawayFee"),
    @XmlEnumValue("ApplicationFee")
    APPLICATION_FEE("ApplicationFee"),
    @XmlEnumValue("Assessment/LicenseTax")
    ASSESSMENT_LICENSE_TAX("Assessment/LicenseTax"),
    @XmlEnumValue("BanquetServiceFee")
    BANQUET_SERVICE_FEE("BanquetServiceFee"),
    @XmlEnumValue("BedTax")
    BED_TAX("BedTax"),
    @XmlEnumValue("ChildRollawayCharge")
    CHILD_ROLLAWAY_CHARGE("ChildRollawayCharge"),
    @XmlEnumValue("CityHotelFee")
    CITY_HOTEL_FEE("CityHotelFee"),
    @XmlEnumValue("CityTax")
    CITY_TAX("CityTax"),
    @XmlEnumValue("ConventionTax")
    CONVENTION_TAX("ConventionTax"),
    @XmlEnumValue("CountryTax")
    COUNTRY_TAX("CountryTax"),
    @XmlEnumValue("CountyTax")
    COUNTY_TAX("CountyTax"),
    @XmlEnumValue("CribFee")
    CRIB_FEE("CribFee"),
    @XmlEnumValue("EarlyCheckoutFee")
    EARLY_CHECKOUT_FEE("EarlyCheckoutFee"),
    @XmlEnumValue("EnergyTax")
    ENERGY_TAX("EnergyTax"),
    @XmlEnumValue("Exempt")
    EXEMPT("Exempt"),
    @XmlEnumValue("ExpressHandlingFee")
    EXPRESS_HANDLING_FEE("ExpressHandlingFee"),
    @XmlEnumValue("ExtraChildCharge")
    EXTRA_CHILD_CHARGE("ExtraChildCharge"),
    @XmlEnumValue("ExtraPersonCharge")
    EXTRA_PERSON_CHARGE("ExtraPersonCharge"),
    @XmlEnumValue("FederalTax")
    FEDERAL_TAX("FederalTax"),
    @XmlEnumValue("FoodAndBeverageTax")
    FOOD_AND_BEVERAGE_TAX("FoodAndBeverageTax"),
    @XmlEnumValue("FoodAndBeverageGratuity")
    FOOD_AND_BEVERAGE_GRATUITY("FoodAndBeverageGratuity"),
    @XmlEnumValue("GoodsAndServicesTax(GST)")
    GOODS_AND_SERVICES_TAX_GST("GoodsAndServicesTax(GST)"),
    @XmlEnumValue("InsurancePremiumTax")
    INSURANCE_PREMIUM_TAX("InsurancePremiumTax"),
    @XmlEnumValue("LocalFee")
    LOCAL_FEE("LocalFee"),
    @XmlEnumValue("LodgingTax")
    LODGING_TAX("LodgingTax"),
    @XmlEnumValue("MaintenanceFee")
    MAINTENANCE_FEE("MaintenanceFee"),
    @XmlEnumValue("Miscellaneous")
    MISCELLANEOUS("Miscellaneous"),
    @XmlEnumValue("NationalGovernmentTax")
    NATIONAL_GOVERNMENT_TAX("NationalGovernmentTax"),
    @XmlEnumValue("NotKnown")
    NOT_KNOWN("NotKnown"),
    @XmlEnumValue("OccupancyTax")
    OCCUPANCY_TAX("OccupancyTax"),
    @XmlEnumValue("PackageFee")
    PACKAGE_FEE("PackageFee"),
    @XmlEnumValue("PetSanitationFee")
    PET_SANITATION_FEE("PetSanitationFee"),
    @XmlEnumValue("ResortFee")
    RESORT_FEE("ResortFee"),
    @XmlEnumValue("RollawayFee")
    ROLLAWAY_FEE("RollawayFee"),
    @XmlEnumValue("RoomServiceFee")
    ROOM_SERVICE_FEE("RoomServiceFee"),
    @XmlEnumValue("RoomTax")
    ROOM_TAX("RoomTax"),
    @XmlEnumValue("SalesTax")
    SALES_TAX("SalesTax"),
    @XmlEnumValue("ServiceCharge")
    SERVICE_CHARGE("ServiceCharge"),
    @XmlEnumValue("Standard")
    STANDARD("Standard"),
    @XmlEnumValue("StateTax")
    STATE_TAX("StateTax"),
    @XmlEnumValue("Surcharge")
    SURCHARGE("Surcharge"),
    @XmlEnumValue("SurplusLinesTax")
    SURPLUS_LINES_TAX("SurplusLinesTax"),
    @XmlEnumValue("TotalTax")
    TOTAL_TAX("TotalTax"),
    @XmlEnumValue("TourismTax")
    TOURISM_TAX("TourismTax"),
    @XmlEnumValue("ValueAddedTax(VAT)")
    VALUE_ADDED_TAX_VAT("ValueAddedTax(VAT)"),
    @XmlEnumValue("VAT/GSTTax")
    VAT_GST_TAX("VAT/GSTTax"),
    @XmlEnumValue("Zero-Rated")
    ZERO_RATED("Zero-Rated"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListFeeTaxTypeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListFeeTaxTypeBase fromValue(String v) {
        for (ListFeeTaxTypeBase c: ListFeeTaxTypeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
