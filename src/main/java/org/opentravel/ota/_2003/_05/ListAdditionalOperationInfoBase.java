
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_AdditionalOperationInfo_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_AdditionalOperationInfo_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AfterHoursAvailable"/&amp;gt;
 *     &amp;lt;enumeration value="AfterHoursConditional"/&amp;gt;
 *     &amp;lt;enumeration value="AfterHoursPickups"/&amp;gt;
 *     &amp;lt;enumeration value="AfterHoursReturns"/&amp;gt;
 *     &amp;lt;enumeration value="LimitedAvailability"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_AdditionalOperationInfo_Base")
@XmlEnum
public enum ListAdditionalOperationInfoBase {

    @XmlEnumValue("AfterHoursAvailable")
    AFTER_HOURS_AVAILABLE("AfterHoursAvailable"),
    @XmlEnumValue("AfterHoursConditional")
    AFTER_HOURS_CONDITIONAL("AfterHoursConditional"),
    @XmlEnumValue("AfterHoursPickups")
    AFTER_HOURS_PICKUPS("AfterHoursPickups"),
    @XmlEnumValue("AfterHoursReturns")
    AFTER_HOURS_RETURNS("AfterHoursReturns"),
    @XmlEnumValue("LimitedAvailability")
    LIMITED_AVAILABILITY("LimitedAvailability"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListAdditionalOperationInfoBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListAdditionalOperationInfoBase fromValue(String v) {
        for (ListAdditionalOperationInfoBase c: ListAdditionalOperationInfoBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
