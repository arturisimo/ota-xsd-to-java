
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Choice between summary and detailed code extension information.
 * 
 * &lt;p&gt;Clase Java para CodeListExtension complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CodeListExtension"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;element name="Summary" type="{http://www.opentravel.org/OTA/2003/05}CodeListSummaryExtension" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Detail" type="{http://www.opentravel.org/OTA/2003/05}CodeListDetailExtension" minOccurs="0"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attribute name="ChargeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="ProximityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="QuantityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="ScheduleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CodeListExtension", propOrder = {
    "summary",
    "detail"
})
public class CodeListExtension {

    @XmlElement(name = "Summary")
    protected CodeListSummaryExtension summary;
    @XmlElement(name = "Detail")
    protected CodeListDetailExtension detail;
    @XmlAttribute(name = "ChargeInd")
    protected Boolean chargeInd;
    @XmlAttribute(name = "ProximityInd")
    protected Boolean proximityInd;
    @XmlAttribute(name = "QuantityInd")
    protected Boolean quantityInd;
    @XmlAttribute(name = "ScheduleInd")
    protected Boolean scheduleInd;

    /**
     * Obtiene el valor de la propiedad summary.
     * 
     * @return
     *     possible object is
     *     {@link CodeListSummaryExtension }
     *     
     */
    public CodeListSummaryExtension getSummary() {
        return summary;
    }

    /**
     * Define el valor de la propiedad summary.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeListSummaryExtension }
     *     
     */
    public void setSummary(CodeListSummaryExtension value) {
        this.summary = value;
    }

    /**
     * Obtiene el valor de la propiedad detail.
     * 
     * @return
     *     possible object is
     *     {@link CodeListDetailExtension }
     *     
     */
    public CodeListDetailExtension getDetail() {
        return detail;
    }

    /**
     * Define el valor de la propiedad detail.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeListDetailExtension }
     *     
     */
    public void setDetail(CodeListDetailExtension value) {
        this.detail = value;
    }

    /**
     * Obtiene el valor de la propiedad chargeInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChargeInd() {
        return chargeInd;
    }

    /**
     * Define el valor de la propiedad chargeInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChargeInd(Boolean value) {
        this.chargeInd = value;
    }

    /**
     * Obtiene el valor de la propiedad proximityInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProximityInd() {
        return proximityInd;
    }

    /**
     * Define el valor de la propiedad proximityInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProximityInd(Boolean value) {
        this.proximityInd = value;
    }

    /**
     * Obtiene el valor de la propiedad quantityInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isQuantityInd() {
        return quantityInd;
    }

    /**
     * Define el valor de la propiedad quantityInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setQuantityInd(Boolean value) {
        this.quantityInd = value;
    }

    /**
     * Obtiene el valor de la propiedad scheduleInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isScheduleInd() {
        return scheduleInd;
    }

    /**
     * Define el valor de la propiedad scheduleInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setScheduleInd(Boolean value) {
        this.scheduleInd = value;
    }

}
