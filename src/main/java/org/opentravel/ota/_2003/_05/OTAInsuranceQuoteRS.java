
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="PlanForQuoteRS" maxOccurs="unbounded"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="QuoteDetail"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="ProviderCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType"/&amp;gt;
 *                               &amp;lt;element name="QuoteDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="PlanRestrictions" type="{http://www.opentravel.org/OTA/2003/05}PlanRestrictionType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="BookingDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EffectiveExpireOptionalDateGroup"/&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="InsCoverageDetail" type="{http://www.opentravel.org/OTA/2003/05}InsCoverageDetailType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="PlanCost" type="{http://www.opentravel.org/OTA/2003/05}PlanCostType"/&amp;gt;
 *                     &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PlanID_Group"/&amp;gt;
 *                   &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "planForQuoteRS",
    "errors"
})
@XmlRootElement(name = "OTA_InsuranceQuoteRS")
public class OTAInsuranceQuoteRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "PlanForQuoteRS")
    protected List<OTAInsuranceQuoteRS.PlanForQuoteRS> planForQuoteRS;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Gets the value of the planForQuoteRS property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the planForQuoteRS property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPlanForQuoteRS().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTAInsuranceQuoteRS.PlanForQuoteRS }
     * 
     * 
     */
    public List<OTAInsuranceQuoteRS.PlanForQuoteRS> getPlanForQuoteRS() {
        if (planForQuoteRS == null) {
            planForQuoteRS = new ArrayList<OTAInsuranceQuoteRS.PlanForQuoteRS>();
        }
        return this.planForQuoteRS;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="QuoteDetail"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ProviderCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType"/&amp;gt;
     *                   &amp;lt;element name="QuoteDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="PlanRestrictions" type="{http://www.opentravel.org/OTA/2003/05}PlanRestrictionType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="BookingDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EffectiveExpireOptionalDateGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="InsCoverageDetail" type="{http://www.opentravel.org/OTA/2003/05}InsCoverageDetailType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="PlanCost" type="{http://www.opentravel.org/OTA/2003/05}PlanCostType"/&amp;gt;
     *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="9" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PlanID_Group"/&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "quoteDetail",
        "insCoverageDetail",
        "planCost",
        "contact"
    })
    public static class PlanForQuoteRS {

        @XmlElement(name = "QuoteDetail", required = true)
        protected OTAInsuranceQuoteRS.PlanForQuoteRS.QuoteDetail quoteDetail;
        @XmlElement(name = "InsCoverageDetail")
        protected InsCoverageDetailType insCoverageDetail;
        @XmlElement(name = "PlanCost", required = true)
        protected PlanCostType planCost;
        @XmlElement(name = "Contact")
        protected List<ContactPersonType> contact;
        @XmlAttribute(name = "RPH")
        protected String rph;
        @XmlAttribute(name = "PlanID", required = true)
        protected String planID;
        @XmlAttribute(name = "Name")
        protected String name;
        @XmlAttribute(name = "Type")
        protected String type;
        @XmlAttribute(name = "TypeID")
        protected String typeID;

        /**
         * Obtiene el valor de la propiedad quoteDetail.
         * 
         * @return
         *     possible object is
         *     {@link OTAInsuranceQuoteRS.PlanForQuoteRS.QuoteDetail }
         *     
         */
        public OTAInsuranceQuoteRS.PlanForQuoteRS.QuoteDetail getQuoteDetail() {
            return quoteDetail;
        }

        /**
         * Define el valor de la propiedad quoteDetail.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAInsuranceQuoteRS.PlanForQuoteRS.QuoteDetail }
         *     
         */
        public void setQuoteDetail(OTAInsuranceQuoteRS.PlanForQuoteRS.QuoteDetail value) {
            this.quoteDetail = value;
        }

        /**
         * Obtiene el valor de la propiedad insCoverageDetail.
         * 
         * @return
         *     possible object is
         *     {@link InsCoverageDetailType }
         *     
         */
        public InsCoverageDetailType getInsCoverageDetail() {
            return insCoverageDetail;
        }

        /**
         * Define el valor de la propiedad insCoverageDetail.
         * 
         * @param value
         *     allowed object is
         *     {@link InsCoverageDetailType }
         *     
         */
        public void setInsCoverageDetail(InsCoverageDetailType value) {
            this.insCoverageDetail = value;
        }

        /**
         * Obtiene el valor de la propiedad planCost.
         * 
         * @return
         *     possible object is
         *     {@link PlanCostType }
         *     
         */
        public PlanCostType getPlanCost() {
            return planCost;
        }

        /**
         * Define el valor de la propiedad planCost.
         * 
         * @param value
         *     allowed object is
         *     {@link PlanCostType }
         *     
         */
        public void setPlanCost(PlanCostType value) {
            this.planCost = value;
        }

        /**
         * Gets the value of the contact property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contact property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getContact().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ContactPersonType }
         * 
         * 
         */
        public List<ContactPersonType> getContact() {
            if (contact == null) {
                contact = new ArrayList<ContactPersonType>();
            }
            return this.contact;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

        /**
         * Obtiene el valor de la propiedad planID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlanID() {
            return planID;
        }

        /**
         * Define el valor de la propiedad planID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlanID(String value) {
            this.planID = value;
        }

        /**
         * Obtiene el valor de la propiedad name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad typeID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTypeID() {
            return typeID;
        }

        /**
         * Define el valor de la propiedad typeID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTypeID(String value) {
            this.typeID = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ProviderCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType"/&amp;gt;
         *         &amp;lt;element name="QuoteDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="PlanRestrictions" type="{http://www.opentravel.org/OTA/2003/05}PlanRestrictionType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="BookingDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EffectiveExpireOptionalDateGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "providerCompany",
            "quoteDetailURL",
            "planRestrictions",
            "bookingDetailURL"
        })
        public static class QuoteDetail {

            @XmlElement(name = "ProviderCompany", required = true)
            protected CompanyNameType providerCompany;
            @XmlElement(name = "QuoteDetailURL")
            protected URLType quoteDetailURL;
            @XmlElement(name = "PlanRestrictions")
            protected PlanRestrictionType planRestrictions;
            @XmlElement(name = "BookingDetailURL")
            protected URLType bookingDetailURL;
            @XmlAttribute(name = "EffectiveDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar effectiveDate;
            @XmlAttribute(name = "ExpireDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar expireDate;
            @XmlAttribute(name = "ExpireDateExclusiveInd")
            protected Boolean expireDateExclusiveInd;

            /**
             * Obtiene el valor de la propiedad providerCompany.
             * 
             * @return
             *     possible object is
             *     {@link CompanyNameType }
             *     
             */
            public CompanyNameType getProviderCompany() {
                return providerCompany;
            }

            /**
             * Define el valor de la propiedad providerCompany.
             * 
             * @param value
             *     allowed object is
             *     {@link CompanyNameType }
             *     
             */
            public void setProviderCompany(CompanyNameType value) {
                this.providerCompany = value;
            }

            /**
             * Obtiene el valor de la propiedad quoteDetailURL.
             * 
             * @return
             *     possible object is
             *     {@link URLType }
             *     
             */
            public URLType getQuoteDetailURL() {
                return quoteDetailURL;
            }

            /**
             * Define el valor de la propiedad quoteDetailURL.
             * 
             * @param value
             *     allowed object is
             *     {@link URLType }
             *     
             */
            public void setQuoteDetailURL(URLType value) {
                this.quoteDetailURL = value;
            }

            /**
             * Obtiene el valor de la propiedad planRestrictions.
             * 
             * @return
             *     possible object is
             *     {@link PlanRestrictionType }
             *     
             */
            public PlanRestrictionType getPlanRestrictions() {
                return planRestrictions;
            }

            /**
             * Define el valor de la propiedad planRestrictions.
             * 
             * @param value
             *     allowed object is
             *     {@link PlanRestrictionType }
             *     
             */
            public void setPlanRestrictions(PlanRestrictionType value) {
                this.planRestrictions = value;
            }

            /**
             * Obtiene el valor de la propiedad bookingDetailURL.
             * 
             * @return
             *     possible object is
             *     {@link URLType }
             *     
             */
            public URLType getBookingDetailURL() {
                return bookingDetailURL;
            }

            /**
             * Define el valor de la propiedad bookingDetailURL.
             * 
             * @param value
             *     allowed object is
             *     {@link URLType }
             *     
             */
            public void setBookingDetailURL(URLType value) {
                this.bookingDetailURL = value;
            }

            /**
             * Obtiene el valor de la propiedad effectiveDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getEffectiveDate() {
                return effectiveDate;
            }

            /**
             * Define el valor de la propiedad effectiveDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setEffectiveDate(XMLGregorianCalendar value) {
                this.effectiveDate = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getExpireDate() {
                return expireDate;
            }

            /**
             * Define el valor de la propiedad expireDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setExpireDate(XMLGregorianCalendar value) {
                this.expireDate = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDateExclusiveInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isExpireDateExclusiveInd() {
                return expireDateExclusiveInd;
            }

            /**
             * Define el valor de la propiedad expireDateExclusiveInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setExpireDateExclusiveInd(Boolean value) {
                this.expireDateExclusiveInd = value;
            }

        }

    }

}
