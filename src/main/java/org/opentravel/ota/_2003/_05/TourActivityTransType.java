
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Detailed pickup and drop off information for the tour/activity.
 * 
 * &lt;p&gt;Clase Java para TourActivityTransType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TourActivityTransType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Charges" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Schedule" type="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="DateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *       &amp;lt;attribute name="LocationName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="Directions" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="MeetingLocation" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="OtherInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="PickupInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="DropoffInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TourActivityTransType", propOrder = {
    "charges",
    "schedule"
})
public class TourActivityTransType {

    @XmlElement(name = "Charges")
    protected TourActivityChargeType charges;
    @XmlElement(name = "Schedule")
    protected OperationScheduleType schedule;
    @XmlAttribute(name = "DateTime")
    protected String dateTime;
    @XmlAttribute(name = "LocationName", required = true)
    protected String locationName;
    @XmlAttribute(name = "Directions")
    protected String directions;
    @XmlAttribute(name = "MeetingLocation")
    protected String meetingLocation;
    @XmlAttribute(name = "OtherInfo")
    protected String otherInfo;
    @XmlAttribute(name = "PickupInd")
    protected Boolean pickupInd;
    @XmlAttribute(name = "DropoffInd")
    protected Boolean dropoffInd;

    /**
     * Obtiene el valor de la propiedad charges.
     * 
     * @return
     *     possible object is
     *     {@link TourActivityChargeType }
     *     
     */
    public TourActivityChargeType getCharges() {
        return charges;
    }

    /**
     * Define el valor de la propiedad charges.
     * 
     * @param value
     *     allowed object is
     *     {@link TourActivityChargeType }
     *     
     */
    public void setCharges(TourActivityChargeType value) {
        this.charges = value;
    }

    /**
     * Obtiene el valor de la propiedad schedule.
     * 
     * @return
     *     possible object is
     *     {@link OperationScheduleType }
     *     
     */
    public OperationScheduleType getSchedule() {
        return schedule;
    }

    /**
     * Define el valor de la propiedad schedule.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationScheduleType }
     *     
     */
    public void setSchedule(OperationScheduleType value) {
        this.schedule = value;
    }

    /**
     * Obtiene el valor de la propiedad dateTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * Define el valor de la propiedad dateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTime(String value) {
        this.dateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad locationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Define el valor de la propiedad locationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationName(String value) {
        this.locationName = value;
    }

    /**
     * Obtiene el valor de la propiedad directions.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirections() {
        return directions;
    }

    /**
     * Define el valor de la propiedad directions.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirections(String value) {
        this.directions = value;
    }

    /**
     * Obtiene el valor de la propiedad meetingLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeetingLocation() {
        return meetingLocation;
    }

    /**
     * Define el valor de la propiedad meetingLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeetingLocation(String value) {
        this.meetingLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad otherInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherInfo() {
        return otherInfo;
    }

    /**
     * Define el valor de la propiedad otherInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherInfo(String value) {
        this.otherInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad pickupInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPickupInd() {
        return pickupInd;
    }

    /**
     * Define el valor de la propiedad pickupInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPickupInd(Boolean value) {
        this.pickupInd = value;
    }

    /**
     * Obtiene el valor de la propiedad dropoffInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDropoffInd() {
        return dropoffInd;
    }

    /**
     * Define el valor de la propiedad dropoffInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDropoffInd(Boolean value) {
        this.dropoffInd = value;
    }

}
