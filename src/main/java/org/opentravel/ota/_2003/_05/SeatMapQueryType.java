
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Object to hold criteria used to generate one or more seat maps.
 * 
 * &lt;p&gt;Clase Java para SeatMapQueryType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="SeatMapQueryType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="FlightInfo" maxOccurs="99"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Airline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;choice&amp;gt;
 *                     &amp;lt;element name="OriginDestination" type="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="AirItineraryDetail" type="{http://www.opentravel.org/OTA/2003/05}AirItineraryType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;/choice&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="FlightNumber" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
 *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Equipment" type="{http://www.opentravel.org/OTA/2003/05}EquipmentType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BookingReferenceID" type="{http://www.opentravel.org/OTA/2003/05}BookingReferenceType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CabinClass" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CabinClassQueryType"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Features" type="{http://www.opentravel.org/OTA/2003/05}SeatFeaturesType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SpecificSeatInfo" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="DeckLevel" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                 &amp;lt;attribute name="RowNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
 *                 &amp;lt;attribute name="SeatInRow" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength1" /&amp;gt;
 *                 &amp;lt;attribute name="SeatNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SeatDetails" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CabinClass" maxOccurs="5" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;simpleContent&amp;gt;
 *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CabinClassQueryType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/simpleContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ResBookDesignations" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ResBookDesignation" maxOccurs="10"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}FareBasisCodeType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="SeatUpgradeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Status" type="{http://www.opentravel.org/OTA/2003/05}StatusType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Traveler" type="{http://www.opentravel.org/OTA/2003/05}TravelerInfoType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Zone" type="{http://www.opentravel.org/OTA/2003/05}SeatZoneQueryType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="SmokingAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="TotalReqSeatQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="ChangeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SeatMapQueryType", propOrder = {
    "flightInfo",
    "equipment",
    "bookingReferenceID",
    "cabinClass",
    "features",
    "specificSeatInfo",
    "seatDetails",
    "status",
    "traveler",
    "zone",
    "tpaExtensions"
})
public class SeatMapQueryType {

    @XmlElement(name = "FlightInfo", required = true)
    protected List<SeatMapQueryType.FlightInfo> flightInfo;
    @XmlElement(name = "Equipment")
    protected List<EquipmentType> equipment;
    @XmlElement(name = "BookingReferenceID")
    protected List<BookingReferenceType> bookingReferenceID;
    @XmlElement(name = "CabinClass")
    protected List<SeatMapQueryType.CabinClass> cabinClass;
    @XmlElement(name = "Features")
    protected List<SeatFeaturesType> features;
    @XmlElement(name = "SpecificSeatInfo")
    protected List<SeatMapQueryType.SpecificSeatInfo> specificSeatInfo;
    @XmlElement(name = "SeatDetails")
    protected SeatMapQueryType.SeatDetails seatDetails;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "Traveler")
    protected List<TravelerInfoType> traveler;
    @XmlElement(name = "Zone")
    protected List<SeatZoneQueryType> zone;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "SmokingAllowed")
    protected Boolean smokingAllowed;
    @XmlAttribute(name = "TotalReqSeatQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger totalReqSeatQty;
    @XmlAttribute(name = "ChangeInd")
    protected Boolean changeInd;

    /**
     * Gets the value of the flightInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFlightInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link SeatMapQueryType.FlightInfo }
     * 
     * 
     */
    public List<SeatMapQueryType.FlightInfo> getFlightInfo() {
        if (flightInfo == null) {
            flightInfo = new ArrayList<SeatMapQueryType.FlightInfo>();
        }
        return this.flightInfo;
    }

    /**
     * Gets the value of the equipment property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the equipment property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getEquipment().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentType }
     * 
     * 
     */
    public List<EquipmentType> getEquipment() {
        if (equipment == null) {
            equipment = new ArrayList<EquipmentType>();
        }
        return this.equipment;
    }

    /**
     * Gets the value of the bookingReferenceID property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the bookingReferenceID property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getBookingReferenceID().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link BookingReferenceType }
     * 
     * 
     */
    public List<BookingReferenceType> getBookingReferenceID() {
        if (bookingReferenceID == null) {
            bookingReferenceID = new ArrayList<BookingReferenceType>();
        }
        return this.bookingReferenceID;
    }

    /**
     * Gets the value of the cabinClass property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the cabinClass property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCabinClass().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link SeatMapQueryType.CabinClass }
     * 
     * 
     */
    public List<SeatMapQueryType.CabinClass> getCabinClass() {
        if (cabinClass == null) {
            cabinClass = new ArrayList<SeatMapQueryType.CabinClass>();
        }
        return this.cabinClass;
    }

    /**
     * Gets the value of the features property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the features property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFeatures().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link SeatFeaturesType }
     * 
     * 
     */
    public List<SeatFeaturesType> getFeatures() {
        if (features == null) {
            features = new ArrayList<SeatFeaturesType>();
        }
        return this.features;
    }

    /**
     * Gets the value of the specificSeatInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the specificSeatInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSpecificSeatInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link SeatMapQueryType.SpecificSeatInfo }
     * 
     * 
     */
    public List<SeatMapQueryType.SpecificSeatInfo> getSpecificSeatInfo() {
        if (specificSeatInfo == null) {
            specificSeatInfo = new ArrayList<SeatMapQueryType.SpecificSeatInfo>();
        }
        return this.specificSeatInfo;
    }

    /**
     * Obtiene el valor de la propiedad seatDetails.
     * 
     * @return
     *     possible object is
     *     {@link SeatMapQueryType.SeatDetails }
     *     
     */
    public SeatMapQueryType.SeatDetails getSeatDetails() {
        return seatDetails;
    }

    /**
     * Define el valor de la propiedad seatDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatMapQueryType.SeatDetails }
     *     
     */
    public void setSeatDetails(SeatMapQueryType.SeatDetails value) {
        this.seatDetails = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the status property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getStatus().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the traveler property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the traveler property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTraveler().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TravelerInfoType }
     * 
     * 
     */
    public List<TravelerInfoType> getTraveler() {
        if (traveler == null) {
            traveler = new ArrayList<TravelerInfoType>();
        }
        return this.traveler;
    }

    /**
     * Gets the value of the zone property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the zone property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getZone().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link SeatZoneQueryType }
     * 
     * 
     */
    public List<SeatZoneQueryType> getZone() {
        if (zone == null) {
            zone = new ArrayList<SeatZoneQueryType>();
        }
        return this.zone;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad smokingAllowed.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSmokingAllowed() {
        return smokingAllowed;
    }

    /**
     * Define el valor de la propiedad smokingAllowed.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSmokingAllowed(Boolean value) {
        this.smokingAllowed = value;
    }

    /**
     * Obtiene el valor de la propiedad totalReqSeatQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalReqSeatQty() {
        return totalReqSeatQty;
    }

    /**
     * Define el valor de la propiedad totalReqSeatQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalReqSeatQty(BigInteger value) {
        this.totalReqSeatQty = value;
    }

    /**
     * Obtiene el valor de la propiedad changeInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChangeInd() {
        return changeInd;
    }

    /**
     * Define el valor de la propiedad changeInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChangeInd(Boolean value) {
        this.changeInd = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CabinClassQueryType"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class CabinClass
        extends CabinClassQueryType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Airline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *         &amp;lt;choice&amp;gt;
     *           &amp;lt;element name="OriginDestination" type="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType" minOccurs="0"/&amp;gt;
     *           &amp;lt;element name="AirItineraryDetail" type="{http://www.opentravel.org/OTA/2003/05}AirItineraryType" minOccurs="0"/&amp;gt;
     *         &amp;lt;/choice&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="FlightNumber" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
     *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airline",
        "originDestination",
        "airItineraryDetail"
    })
    public static class FlightInfo {

        @XmlElement(name = "Airline")
        protected CompanyNameType airline;
        @XmlElement(name = "OriginDestination")
        protected OriginDestinationInformationType originDestination;
        @XmlElement(name = "AirItineraryDetail")
        protected AirItineraryType airItineraryDetail;
        @XmlAttribute(name = "FlightNumber", required = true)
        protected String flightNumber;
        @XmlAttribute(name = "TravelerRPH")
        protected String travelerRPH;

        /**
         * Obtiene el valor de la propiedad airline.
         * 
         * @return
         *     possible object is
         *     {@link CompanyNameType }
         *     
         */
        public CompanyNameType getAirline() {
            return airline;
        }

        /**
         * Define el valor de la propiedad airline.
         * 
         * @param value
         *     allowed object is
         *     {@link CompanyNameType }
         *     
         */
        public void setAirline(CompanyNameType value) {
            this.airline = value;
        }

        /**
         * Obtiene el valor de la propiedad originDestination.
         * 
         * @return
         *     possible object is
         *     {@link OriginDestinationInformationType }
         *     
         */
        public OriginDestinationInformationType getOriginDestination() {
            return originDestination;
        }

        /**
         * Define el valor de la propiedad originDestination.
         * 
         * @param value
         *     allowed object is
         *     {@link OriginDestinationInformationType }
         *     
         */
        public void setOriginDestination(OriginDestinationInformationType value) {
            this.originDestination = value;
        }

        /**
         * Obtiene el valor de la propiedad airItineraryDetail.
         * 
         * @return
         *     possible object is
         *     {@link AirItineraryType }
         *     
         */
        public AirItineraryType getAirItineraryDetail() {
            return airItineraryDetail;
        }

        /**
         * Define el valor de la propiedad airItineraryDetail.
         * 
         * @param value
         *     allowed object is
         *     {@link AirItineraryType }
         *     
         */
        public void setAirItineraryDetail(AirItineraryType value) {
            this.airItineraryDetail = value;
        }

        /**
         * Obtiene el valor de la propiedad flightNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightNumber() {
            return flightNumber;
        }

        /**
         * Define el valor de la propiedad flightNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightNumber(String value) {
            this.flightNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad travelerRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelerRPH() {
            return travelerRPH;
        }

        /**
         * Define el valor de la propiedad travelerRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelerRPH(String value) {
            this.travelerRPH = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CabinClass" maxOccurs="5" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;simpleContent&amp;gt;
     *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CabinClassQueryType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/simpleContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ResBookDesignations" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ResBookDesignation" maxOccurs="10"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}FareBasisCodeType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="SeatUpgradeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cabinClass",
        "resBookDesignations",
        "fareBasisCode"
    })
    public static class SeatDetails {

        @XmlElement(name = "CabinClass")
        protected List<SeatMapQueryType.SeatDetails.CabinClass> cabinClass;
        @XmlElement(name = "ResBookDesignations")
        protected SeatMapQueryType.SeatDetails.ResBookDesignations resBookDesignations;
        @XmlElement(name = "FareBasisCode")
        protected FareBasisCodeType fareBasisCode;
        @XmlAttribute(name = "SeatUpgradeInd")
        protected Boolean seatUpgradeInd;

        /**
         * Gets the value of the cabinClass property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the cabinClass property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCabinClass().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link SeatMapQueryType.SeatDetails.CabinClass }
         * 
         * 
         */
        public List<SeatMapQueryType.SeatDetails.CabinClass> getCabinClass() {
            if (cabinClass == null) {
                cabinClass = new ArrayList<SeatMapQueryType.SeatDetails.CabinClass>();
            }
            return this.cabinClass;
        }

        /**
         * Obtiene el valor de la propiedad resBookDesignations.
         * 
         * @return
         *     possible object is
         *     {@link SeatMapQueryType.SeatDetails.ResBookDesignations }
         *     
         */
        public SeatMapQueryType.SeatDetails.ResBookDesignations getResBookDesignations() {
            return resBookDesignations;
        }

        /**
         * Define el valor de la propiedad resBookDesignations.
         * 
         * @param value
         *     allowed object is
         *     {@link SeatMapQueryType.SeatDetails.ResBookDesignations }
         *     
         */
        public void setResBookDesignations(SeatMapQueryType.SeatDetails.ResBookDesignations value) {
            this.resBookDesignations = value;
        }

        /**
         * Obtiene el valor de la propiedad fareBasisCode.
         * 
         * @return
         *     possible object is
         *     {@link FareBasisCodeType }
         *     
         */
        public FareBasisCodeType getFareBasisCode() {
            return fareBasisCode;
        }

        /**
         * Define el valor de la propiedad fareBasisCode.
         * 
         * @param value
         *     allowed object is
         *     {@link FareBasisCodeType }
         *     
         */
        public void setFareBasisCode(FareBasisCodeType value) {
            this.fareBasisCode = value;
        }

        /**
         * Obtiene el valor de la propiedad seatUpgradeInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSeatUpgradeInd() {
            return seatUpgradeInd;
        }

        /**
         * Define el valor de la propiedad seatUpgradeInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSeatUpgradeInd(Boolean value) {
            this.seatUpgradeInd = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;simpleContent&amp;gt;
         *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CabinClassQueryType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/simpleContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CabinClass
            extends CabinClassQueryType
        {


        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ResBookDesignation" maxOccurs="10"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "resBookDesignation"
        })
        public static class ResBookDesignations {

            @XmlElement(name = "ResBookDesignation", required = true)
            protected List<SeatMapQueryType.SeatDetails.ResBookDesignations.ResBookDesignation> resBookDesignation;

            /**
             * Gets the value of the resBookDesignation property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the resBookDesignation property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getResBookDesignation().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link SeatMapQueryType.SeatDetails.ResBookDesignations.ResBookDesignation }
             * 
             * 
             */
            public List<SeatMapQueryType.SeatDetails.ResBookDesignations.ResBookDesignation> getResBookDesignation() {
                if (resBookDesignation == null) {
                    resBookDesignation = new ArrayList<SeatMapQueryType.SeatDetails.ResBookDesignations.ResBookDesignation>();
                }
                return this.resBookDesignation;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ResBookDesignation {

                @XmlAttribute(name = "ResBookDesigCode")
                protected String resBookDesigCode;

                /**
                 * Obtiene el valor de la propiedad resBookDesigCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResBookDesigCode() {
                    return resBookDesigCode;
                }

                /**
                 * Define el valor de la propiedad resBookDesigCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResBookDesigCode(String value) {
                    this.resBookDesigCode = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="DeckLevel" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *       &amp;lt;attribute name="RowNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
     *       &amp;lt;attribute name="SeatInRow" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength1" /&amp;gt;
     *       &amp;lt;attribute name="SeatNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="TravelerRPH" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SpecificSeatInfo {

        @XmlAttribute(name = "DeckLevel")
        protected String deckLevel;
        @XmlAttribute(name = "RowNumber")
        protected Integer rowNumber;
        @XmlAttribute(name = "SeatInRow")
        protected String seatInRow;
        @XmlAttribute(name = "SeatNumber")
        protected String seatNumber;
        @XmlAttribute(name = "TravelerRPH")
        @XmlSchemaType(name = "anySimpleType")
        protected String travelerRPH;

        /**
         * Obtiene el valor de la propiedad deckLevel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeckLevel() {
            return deckLevel;
        }

        /**
         * Define el valor de la propiedad deckLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeckLevel(String value) {
            this.deckLevel = value;
        }

        /**
         * Obtiene el valor de la propiedad rowNumber.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRowNumber() {
            return rowNumber;
        }

        /**
         * Define el valor de la propiedad rowNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRowNumber(Integer value) {
            this.rowNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad seatInRow.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSeatInRow() {
            return seatInRow;
        }

        /**
         * Define el valor de la propiedad seatInRow.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSeatInRow(String value) {
            this.seatInRow = value;
        }

        /**
         * Obtiene el valor de la propiedad seatNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSeatNumber() {
            return seatNumber;
        }

        /**
         * Define el valor de la propiedad seatNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSeatNumber(String value) {
            this.seatNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad travelerRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelerRPH() {
            return travelerRPH;
        }

        /**
         * Define el valor de la propiedad travelerRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelerRPH(String value) {
            this.travelerRPH = value;
        }

    }

}
