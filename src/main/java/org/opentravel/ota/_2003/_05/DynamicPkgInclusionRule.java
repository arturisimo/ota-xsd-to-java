
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para DynamicPkgInclusionRule.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="DynamicPkgInclusionRule"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Included"/&amp;gt;
 *     &amp;lt;enumeration value="Optional"/&amp;gt;
 *     &amp;lt;enumeration value="Required"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "DynamicPkgInclusionRule")
@XmlEnum
public enum DynamicPkgInclusionRule {

    @XmlEnumValue("Included")
    INCLUDED("Included"),
    @XmlEnumValue("Optional")
    OPTIONAL("Optional"),
    @XmlEnumValue("Required")
    REQUIRED("Required");
    private final String value;

    DynamicPkgInclusionRule(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DynamicPkgInclusionRule fromValue(String v) {
        for (DynamicPkgInclusionRule c: DynamicPkgInclusionRule.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
