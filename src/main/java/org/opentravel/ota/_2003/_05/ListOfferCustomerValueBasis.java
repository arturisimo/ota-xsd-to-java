
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferCustomerValueBasis.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferCustomerValueBasis"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="ClassOfService"/&amp;gt;
 *     &amp;lt;enumeration value="CorporateClient"/&amp;gt;
 *     &amp;lt;enumeration value="FareClass"/&amp;gt;
 *     &amp;lt;enumeration value="FrequentGuest"/&amp;gt;
 *     &amp;lt;enumeration value="FrequentTraveler"/&amp;gt;
 *     &amp;lt;enumeration value="LoyaltyMemberTimePeriod"/&amp;gt;
 *     &amp;lt;enumeration value="LoyaltyProgramLevel"/&amp;gt;
 *     &amp;lt;enumeration value="PreferredClient"/&amp;gt;
 *     &amp;lt;enumeration value="PremiumOfferPurchase"/&amp;gt;
 *     &amp;lt;enumeration value="TripPrice"/&amp;gt;
 *     &amp;lt;enumeration value="TripPurpose"/&amp;gt;
 *     &amp;lt;enumeration value="UpgradePurchased"/&amp;gt;
 *     &amp;lt;enumeration value="VIP"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferCustomerValueBasis")
@XmlEnum
public enum ListOfferCustomerValueBasis {


    /**
     * Class of service booked by the traveler, e.g. first, business, economy.
     * 
     */
    @XmlEnumValue("ClassOfService")
    CLASS_OF_SERVICE("ClassOfService"),

    /**
     * Traveler is an employee with a preferred or negotiated rate corporate account.
     * 
     */
    @XmlEnumValue("CorporateClient")
    CORPORATE_CLIENT("CorporateClient"),
    @XmlEnumValue("FareClass")
    FARE_CLASS("FareClass"),
    @XmlEnumValue("FrequentGuest")
    FREQUENT_GUEST("FrequentGuest"),
    @XmlEnumValue("FrequentTraveler")
    FREQUENT_TRAVELER("FrequentTraveler"),
    @XmlEnumValue("LoyaltyMemberTimePeriod")
    LOYALTY_MEMBER_TIME_PERIOD("LoyaltyMemberTimePeriod"),
    @XmlEnumValue("LoyaltyProgramLevel")
    LOYALTY_PROGRAM_LEVEL("LoyaltyProgramLevel"),
    @XmlEnumValue("PreferredClient")
    PREFERRED_CLIENT("PreferredClient"),
    @XmlEnumValue("PremiumOfferPurchase")
    PREMIUM_OFFER_PURCHASE("PremiumOfferPurchase"),
    @XmlEnumValue("TripPrice")
    TRIP_PRICE("TripPrice"),
    @XmlEnumValue("TripPurpose")
    TRIP_PURPOSE("TripPurpose"),
    @XmlEnumValue("UpgradePurchased")
    UPGRADE_PURCHASED("UpgradePurchased"),
    VIP("VIP"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferCustomerValueBasis(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferCustomerValueBasis fromValue(String v) {
        for (ListOfferCustomerValueBasis c: ListOfferCustomerValueBasis.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
